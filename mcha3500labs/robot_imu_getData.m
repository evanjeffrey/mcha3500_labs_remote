% robot_imu_getData
close all;
clearvars;
clc;

%% remove any persistent serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end

%% Code to log the potentiometer and read the values into MATLAB
% Create connections to each device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);

% Call the data-logging code. 'compose' appends the correct
% new-line characters to the string
txStr = compose("logIMU");
fprintf(stm_device, txStr);

% Define expected number of samples
N = 2000;       % log for 10 seconds, probably

% Initialise data structure for data
imuData.time = zeros(N, 1);
imuData.imuAngle = zeros(N, 1);
imuData.imuVelocity = zeros(N, 1);
imuData.encAngle = zeros(N, 1);

% Collect the expected number of samples
for i=1:N
    % Read in data from serial connection
    rxStr = fgets(stm_device);
    
    % Separate string into parts
    separatedString = split(rxStr, ',');
    
    % Convert data to numbers and store in data structure
    imuData.time(i) = str2double(separatedString{1});
    imuData.imuAngle(i) = str2double(separatedString{2});
    imuData.imuVelocity(i) = str2double(separatedString{3});
    imuData.encAngle(i) = -str2double(separatedString{4});
end

% Plot results to see what's happening
figure(1)

subplot(3, 1, 1)
plot(imuData.time, imuData.imuAngle, '+')
xlabel("Time")
ylabel("IMU angle")
grid on
grid minor
grid on

subplot(3, 1, 2)
plot(imuData.time, imuData.imuVelocity, '+')
xlabel("Time")
ylabel("IMU velocity")
grid on
grid minor
grid on

subplot(3, 1, 3)
plot(imuData.time, imuData.encAngle, '+')
xlabel("Time")
ylabel("Encoder angle")
grid on
grid minor
grid on

% Teardown
fclose(stm_device);
delete(stm_device);
clear stm_device % Close serial connection and clean up
clear stm_device % Close serial connection and clean up








