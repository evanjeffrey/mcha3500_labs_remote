% handle class called classMPCcontrol

classdef classMPCcontrol < handle
    
    properties
        controlMode         % indicator of active control mode
        u                   % previous control value
        T                   % sampling period of controller
        N_horizon           % MPC horizon length
        A                   % Linearised A matrix for controller
        B                   % Linearised B matrix for controller
        C_r                 % Linearised C matrix for regulation output
        D_r                 % Linearised D matrix for regulation output
        A_int               % A matrix augmented with integrator
        B_int               % B matrix augmented with integrator
        Q                   % Q martix for LQR cost
        R                   % R matrix for LQR cost
        N                   % N matrix for LQR cost
        N_x                 % feed-forward gain for states
        N_u                 % feed-forward gain for inputs
        y_ref               % output regulation reference
        A_z                 % state coefficient for integrator transition dynamics
        B_z                 % input coefficient for integrator transition dynamics
        z                   % integrator state
        A_MPC               % concatenated A matrix over control horizon
        B_MPC               % concatenated B matrix over control horizon
        Q_MPC               % concatenated Q matrix over control horizon
        R_MPC               % concatenated R matrix over control horizon
        N_MPC               % concatenated N matrix over control horizon
        H_MPC               % Hessian of quadratic control cost
        fBar_MPC            % offset coefficient for quadratic control cost
        u_max               % control positive limit
        u_min               % control negative limit
        delta_u_max         % control positive slew rate limit
        delta_u_min         % control negative slew rate limit
        CONST_INEQUAL_A     % inequality constraint A matrix
        CONST_INEQUAL_B     % inequality contsraint B matrix
        CONST_BOUND_UPPER   % control upper bound vector
        CONST_BOUND_LOWER   % control lower bound vector
    end
    
    methods
        
        % function to compute the matrices required for MPC
        function [] = controlInit_regulation(obj, A, B, Q, R, N, T, N_horizon, u_max, u_min, delta_u_max, delta_u_min)
            % update properties
            obj.A = A;
            obj.B = B;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            obj.N_horizon = N_horizon;
            obj.u_max = u_max;
            obj.u_min = u_min;
            obj.delta_u_max = delta_u_max;
            obj.delta_u_min = delta_u_min;
            
            % compute all of the matrices required for MPC
            
            % compute discrete time costs using the provided 'dlqr_cost'
            % function
            [Ad, Bd, Qd, Rd, Nd] = dlqr_cost(A, B, Q, R, N, T);
            
            % compute terminal cost using 'dare' (or otherwise)
            [Qf, ~, ~] = ;
            
            % construct concatinated matrices
            size_x = length(A);
            [~, size_u] = size(B);
            
            % pre-allocate space for results
            obj.A_MPC = zeros(size_x*(N_horizon + 1), size_x);
            obj.B_MPC = zeros(size_x*(N_horizon + 1), N_horizon*size_u);
            obj.Q_MPC = zeros(size_x*(N_horizon + 1), size_x*(N_horizon + 1));
            obj.R_MPC = zeros(N_horizon*size_u, N_horizon*size_u);
            obj.N_MPC = zeros(size_x*(N_horizon + 1), N_horizon*size_u);
            
            % construct matrices by looping through horizon
            obj.A_MPC(1:size_x, :) = eye(size_x);
            obj.Q_MPC((size_x*N_horizon + 1):end, (size_x*N_horizon + 1):end) = Qf;
            for i = 1:N_horizon
                obj.A_MPC((i*(size_x + 1):(i + 1)*size_x), :) = Ad^i;
            end
             
            
        end
        
        
        
    end
    
end