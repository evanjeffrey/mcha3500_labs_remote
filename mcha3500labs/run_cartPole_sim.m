%% run_cartPole_sim
close all;
clearvars;
clc;

%% create cart-pole robot object
cartPoleRobot = classCartPole;
% define parameters
% NOTE: NEED TO GO BACK AND VERIFY THESE YOURSELF WITH SYSTEM
% IDENTIFICATION
cartPoleRobot.mc = 1.9472;
cartPoleRobot.mp = 35e-3;
cartPoleRobot.r = 0.4;
cartPoleRobot.g = 9.8;
cartPoleRobot.l = 0.2864;
cartPoleRobot.b_s = 9.0702;
cartPoleRobot.b_theta = 0.0037;

%
%% create linearised cart-pole robot object
cartPoleRobotLinearised = classCartPoleLinear;
% define parameters
cartPoleRobotLinearised.mc = 1.9472;
cartPoleRobotLinearised.mp = 35e-3;
cartPoleRobotLinearised.r = 0.4;
cartPoleRobotLinearised.g = 9.8;
cartPoleRobotLinearised.l = 0.2864;
cartPoleRobotLinearised.b_s = 9.0702;
cartPoleRobotLinearised.b_theta = 0.0037;

% define how long to run the simulation for
sim.duration = 5;

%% Run simulation before control is implemented
%{
% simulation time
sim_time = [0 sim.duration];

% initial conditions (x(0) = [s(0); theta(0); ds(0); dtheta(0)])
i_c = [0; (pi/180); 0; 0];

% parameters
% alpha = 5*(pi/180);                                                         % angle of ramp (constant)
input.alpha = 0;

% inputs
% F = (cartPoleRobot.mc + cartPoleRobot.mp)*cartPoleRobot.g*sin(alpha);       % input force to the cart (constant)
input.F = 0;

% relative error tolerance of solver
options = odeset('RelTol', 1e-6);

% make a wrapper for the ode solver
% what is a function of time here?
sys_wrap = @(t, x) cartPoleRobot.state_derivative(x, input.F, input.alpha);

% simulate using ode45
[res.t, res.x] = ode45(sys_wrap, sim_time, i_c, options);
%}

input.alpha = 5*(pi/180);

% get the A and B matrices from the cartPoleRobotLinearised object
A_lin = cartPoleRobotLinearised.A();
B_lin = cartPoleRobotLinearised.B();

%% create cart-pole control object

controllerCartPole = classDiscreteLQRcontrol;

%
% control period
T = 0.1;    % corresponds to a 10 [Hz] frequency


% LQR cost
Q = diag([10 1 1 1]);
R = 0.1;
N = zeros(4, 1);

% initialise controller
controllerCartPole.controlInit_regulation(A_lin, B_lin, Q, R, N, T);
% now the gain K is stored in the property 'K' of this class

%}

%% integral control
%
% control period at 200 [Hz]
T = 0.005;

% regulation output
C_r = [1, 0, 0, 0];
D_r = 0;

% LQR gains
Q = diag([10 1 1 1 10]);
R = 0.01;
N = zeros(5, 1);

% initialise controller
controllerCartPole.controlInit_integrator(A_lin, B_lin, C_r, D_r, Q, R, N, T);
% now the controller gain K is stored in the property 'K' of this class
%}

%% run simulation
%
% define initial conditions
sim.s0 = 0;
sim.theta0 = 1*(pi/180);
sim.ds0 = 0;
sim.dtheta0 = 0;
sim.i_c = [sim.s0; sim.theta0; sim.ds0; sim.dtheta0];

sim.time = [0:controllerCartPole.T:5];

% set ode solver tolerance (only needs to be set once, no need to be re-set within the following for loop)
options = odeset('RelTol', 1e-6);

% run ode solver on each defined time interval
res.t = sim.time(1);
res.x = (sim.i_c).';

for i = 1:(length(sim.time) - 1)
    % define ode
    % define initial conditions for next solve
    i_c = res.x(end, :).';
    
    % create wrapper function
    F_in = controllerCartPole.control(i_c);
    sim.ode = @(t, x) cartPoleRobot.state_derivative(x, F_in, input.alpha);
    
    % run with ode45
    [t, x] = ode45(sim.ode, [sim.time(i) sim.time(i+1)], i_c, options);
    
    % concatinate solutions
    res.t = [res.t; t];     % just add more t's onto the column vector with each timestep
    res.x = [res.x; x];     % same here
end
%}

%% plot the results
figure(1)

subplot(4, 1, 1)
plot(res.t, res.x(:, 1), 'LineWidth', 2)
xlabel("Time [s]")
ylabel("s")
grid on
grid minor
legend("s")

subplot(4, 1, 2)
plot(res.t, res.x(:, 2), 'LineWidth', 2)
xlabel("Time [s]")
ylabel("\theta")
grid on
grid minor
legend("\theta")

subplot(4, 1, 3)
plot(res.t, res.x(:, 3), 'LineWidth', 2)
xlabel("Time [s]")
ylabel("ds")
grid on
grid minor
legend("ds")

subplot(4, 1, 4)
plot(res.t, res.x(:, 4), 'LineWidth', 2)
xlabel("Time [s]")
ylabel("d\theta")
grid on
grid minor
legend("d\theta")

%}
