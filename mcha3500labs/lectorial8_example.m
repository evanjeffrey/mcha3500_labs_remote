% lectorial_example
close all;
clearvars;
clc;

%% connect to STM32
% remove any persistent serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end

% create connections to each device
stm_device = serial('COM3', 'Baudrate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);

% reset STM
txStr = compose("reset");
fprintf(stm_device, txStr);

% catch any print return
rxStr = fgets(stm_device);

%% define plant to be controlled
% system parameters
m = 2;
b = 1;

% system model, x = [q; p];
A = [0, (1/m); 0, -(b/m)];
B = [0; 1];

% continuous time model
dx = @(x, u) A*x + B*u;

%% define control law
% continuous time
Q = diag([5, 1]);
R = 1;
N = zeros(2, 1);
K = lqr(A, B, Q, R, N);

% dicrete time
Ts = 0.5;
Kd = lqrd(A, B, Q, R, N, Ts);

%% run simulation
% initial conditions
i_c = [1; 1];
res.x = i_c.'; % just define it for the for loop as the initial condition

% simulation time
sim_time = [0:Ts:10];
res.t = sim_time(1);
res.u = zeros(size(sim_time));  % just declaring the matrix

for i = 1:(length(sim_time) - 1)
    % define ode
    % define initial conditions for next solve
    i_c = res.x(end, :).';
    
    % update controller states on STM32 and get control action
    command_str = strcat("get_control ", num2str(i_c(1))," ", num2str(i_c(2)));
    txStr = compose(command_str);
    fprintf(stm_device, txStr);
    % read in data from serial connection
    rxStr = fgets(stm_device);
    % convert data to numbers and store in data structure
    u = str2double(rxStr);
    
%     u = -Kd*i_c;    % now a static control law
    ode_wrap = @(t, x) dx(x, u);

    % run with ode45
    [t, x] = ode45(ode_wrap, [sim_time(i) sim_time(i+1)], i_c, odeset('RelTol', 1e-6));
    
    % concatinate solutions
    res.t = [res.t; t];     % just add more t's onto the column vector with each timestep
    res.x = [res.x; x];
    res.u(i) = u;
end

%% after running simulation, close the device
fclose(stm_device);
delete(stm_device);
clear stm_device    % close serial connection and clean up

%% plot results
% unpack states
res.q = res.x(:, 1);
res.p = res.x(:, 2);
% res.u = zeros(size(res.t));
% % plot the control also
% for i = 1:length(res.t)
%     res.u(i) = u(res.x(i, :).');
% end

% plot
figure(1)
subplot(3, 1, 1)
plot(res.t, res.q, 'LineWidth', 2)
xlabel("Time")
ylabel("Position")
grid on
grid minor

subplot(3, 1, 2)
stairs(res.t, res.p, 'LineWidth', 2)
xlabel("Time")
ylabel("Momentum")
grid on
grid minor

subplot(3, 1, 3)
stairs(sim_time, res.u, 'LineWidth', 2)
xlabel("Time")
ylabel("Control input")
grid on
grid minor







