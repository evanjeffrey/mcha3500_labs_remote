% robot_runKF
close all;
clearvars;
clc;

% load data
mov1_Data = load("robot_imuData_moving1");

% unpack data
time = mov1_Data.imuData.time;
accAngle = mov1_Data.imuData.imuAngle;
gyroVelocity = mov1_Data.imuData.imuVelocity;
encAngle = mov1_Data.imuData.encAngle;
N = length(time);

% correct angle offset
enc_angle_init = mean(encAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
encAngle = encAngle - enc_angle_init + acc_angle_init;
angle_noise = accAngle - encAngle;

%% set up optimisation routine for the process noise
% define cost
get_angle = @(x) x(:, 2);                   % theta
get_gyro = @(x) x(:, 1) + x(:, 3);          % w + b

cost = @(params) norm(encAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) + norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));

%% run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];      % not sure what these terms are representing

params_opt = fminunc(cost, params0);

%% run Kalman filter for moving data set with the optimal process noise
[x_KF, P_KF, Pp_eig] = runKF(accAngle, gyroVelocity, N, params_opt);

% the state output x_KF will be of the form [dtheta; theta; bias]
kalmanAngle = x_KF(:, 2);
kalmanVelocity = x_KF(:, 1) + x_KF(:, 3);
estErrorCov_1 = Pp_eig(:, 1);
estErrorCov_2 = Pp_eig(:, 2);

%% plot results
% potentiometer angle, Kalman filter predicted angle and acc angle
figure(1)

plot(time, encAngle, '+')
hold on
plot(time, kalmanAngle, 'LineWidth', 2)
plot(time, accAngle, 'LineWidth', 1)
xlabel("Time [s]")
ylabel("Angle [rad]")
grid on
grid minor
legend("Encoder angle", "Kalman estimation", "Acc angle")

% measured gyroscope velocity and Kalman filter estimated velocity
figure(2)
plot(time, gyroVelocity, '+')
hold on
plot(time, kalmanVelocity, 'LineWidth', 2)
xlabel("Time [s]")
ylabel("Angular velocity [rad/s]")
grid on
grid minor
legend("Gyro velocity", "Kalman estimation")

% plot the estimation error covariance
figure(3)
plot(time, estErrorCov_1)
hold on
plot(time, estErrorCov_2)
xlabel("Time [s]")
ylabel("Estimation error covariance eigenvalues")
grid on
grid minor
legend("\lambda_{1}", "\lambda_{2}")


%% run a Kalman filter over the dataset and return the results
function [x_KF, P_KF, Pp_eig] = runKF(angle_accel, velocity_gyro, N, params)
    % unpack params into Cholesky decomposition of Q
    L = [params(1), 0, 0; params(2), params(3), 0; params(4), params(5), params(6)];
    
    % compute process noise Q from L
    Q = L*(L.');
    
    % define continuous time model
    Ac = [0, 0, 0; 1, 0, 0; 0, 0, 0];
    Bc = [];
    C = [0, 1, 0; 1, 0, 1];
    
    % discretise model
    T = 0.005;  % 200 [Hz]
    [Ad, Bd] = c2d(Ac, Bc, T);
    
    % define Kalman filter initial conditions
    % initial state estimate
    xm = [0; 0; 0]; % 3 states since there is noise on the end
    
    % initial estimate error covariance
    Pm = 1*eye(3);
    
    % measurement noise covariance (calc'd earlier)
    R = [0.0497949212514442, 0; 0, 4.61662516236093e-06];
    
    % process noise covariance
%     Q = [1e-6, 0, 0; 0, 1e-6, 0; 0, 0, 1e-6];
    
    % allocate space to save results
    x_KF = zeros(N, 3);
    Pp_eig = zeros(N, 3);
    P_KF = zeros(3, 3, N);
    
    % run Kalman filter
    for i = 1:N
        % pack measurement vector
        y = [angle_accel.'; velocity_gyro.'];
        
        % correction step
        % compute Kalman gain
        Kk = (Pm*(C.'))/(C*Pm*(C.') + R);
        
        % compute corrected state estimate
        % for robot with input, xp = xm + Kk*(y(i) - C*xm - D*u)
        xp = xm + Kk*(y(:, i) - C*xm);
        
        % compute new measurement error covariance
        Pp = (eye(3) - Kk*C)*Pm*((eye(3) - Kk*C).') + Kk*R*(Kk.');
        
        % plot Pp with time to make sure it converges (i.e. save this value)
        
        % prediction step
        % predict next state
        % for robot with input, xm = Ad*xp + Bd*u
        xm = Ad*xp;
        
        % compute prediction error covariance
        Pm = Ad*Pp*(Ad.') + Q;
        
        % store results
        x_KF(i, :) = xp.';
        Pp_eig(i, :) = eig(Pp).';
        P_KF(:, :, i) = Pm;
    end
end




















