% robot_motor_sysid_elec
close all;
clearvars;
clc;

%% motor 1 load data
%{
motor1 = load('robot_motor1_elec_6V_1Hz');
motor1validate = load('robot_motor1_elec_6V_2Hz');  % validation dataset

% unpack data
time = motor1.motor1Data.time;
voltage = motor1.motor1Data.voltage;
velocity = motor1.motor1Data.velocity;
current = motor1.motor1Data.current;

timeVal = motor1validate.motor1Data.time;
voltageVal = motor1validate.motor1Data.voltage;
velocityVal = motor1validate.motor1Data.velocity;
currentVal = motor1validate.motor1Data.current;

% display for motor 1
printString = 'Motor 1';
%}

%% motor 2 load data
%
motor2 = load('robot_motor2_elec_6V_1Hz_newest');
motor2validate = load('robot_motor2_elec_6V_2Hz_newest');  % validation dataset

% unpack data
time = motor2.motor2Data.time;
voltage = motor2.motor2Data.voltage;
velocity = motor2.motor2Data.velocity;
current = motor2.motor2Data.current;

timeVal = motor2validate.motor2Data.time;
voltageVal = motor2validate.motor2Data.voltage;
velocityVal = motor2validate.motor2Data.velocity;
currentVal = motor2validate.motor2Data.current;

% display for motor 1
printString = 'Motor 2';
%}

%% general stuff
disp(printString)
% NOTE: velocity_low is on the low side of the gearbox (velocity of the wheel). But the model we
% have built the sysID from is based on the velocity on the high side of
% the gearbox (the armature of the motor).

% motor 1
figure(1)
subplot(3, 1, 1)        % voltage
plot(time, voltage, '+')
title(printString + ", 1 [Hz] electrical sysID")
xlabel("Time [s]")
ylabel("Voltage [rad/s]")
grid on
grid minor

subplot(3, 1, 2)        % velocity
plot(time, velocity, '+')
xlabel("Time [s]")
ylabel("Velocity [rad/s]")
grid on
grid minor

subplot(3, 1, 3)        % current
plot(time, current, '+')
xlabel("Time [s]")
ylabel("Current [A]")
grid on
grid minor

%% now do sys ID for the first motor
% using least squares, estimate Ra and Kw
% let's work from the formula V_in = Kw*wa + Ra*Ia
phi = [velocity, current];          % now an (Nx2) column vector

% calculate theta by solving the normal equations, using the provided data
theta = (phi.' * phi)\(phi.' * voltage);    % get a (2x1) column vector

% Kw and Ra will form the theta matrix
Kw = theta(1);
Ra = theta(2);

% calculate recovered voltage
voltage_hat = Kw.*velocity + Ra.*current;

%% plot the results of the sysid
figure(1)
subplot(3, 1, 1)
hold on
plot(time, voltage_hat, 'r')
legend("measured", "recovered")

%% print optimal parameters
fprintf("Kw: %f\n", Kw)
fprintf("Ra: %f\n", Ra)

%% validate these parameters on the 2 [Hz] dataset
% use parameters Kw and Ra to calculate voltageVal_hat.
voltageVal_hat = Kw.*velocityVal + Ra.*currentVal;

%% plot results
figure(2)
plot(timeVal, voltageVal, '+')
hold on
plot(timeVal, voltageVal_hat, 'r')
% titleIn = 
title(printString + ", 2 [Hz] Validation electrical sysID")
xlabel("Time [s]")
ylabel("Voltage [V]")
grid on
grid minor
legend("measured", "recovered")





