% class classCartPoleLinear

% state vector x = [s; theta; ds; dtheta];

classdef classCartPoleLinear
    % define properties
    properties
        mc              % mass of the cart
        mp              % mass of the pendulum
        l               % length of the pendulum
        r               % radius of the wheel
        g               % acceleration due to gravity
        b_s             % damping coefficient in the s coordinate
        b_theta         % damping coefficient in the theta coordinate
    end
    
    methods
        function massMatLin = M(obj)
            % computes and returns the linearised mass matrix of the cart
            % pole system
            M1 = obj.mc + obj.mp;
            M2 = obj.l*obj.mp;
            M3 = (obj.l^2)*obj.mp;
            massMatLin = [M1, M2; M2, M3]; % note the cross diagonals are the same element
        end
        
        function dampingMatLin = D(obj)
            % computes and returns the linearised damping matrix of the
            % cart pole system
            D1 = obj.b_s;
            D2 = 0;
            D3 = 0;
            D4 = obj.b_theta;
            dampingMatLin = [D1, D2; D3, D4];
        end
        
        function potGradLin = K(obj)
            % computes and returns the matrix containing linearised
            % coefficients of gravitational forces
            K1 = 0;
            K2 = 0;
            K3 = 0;
            K4 = -obj.mp*obj.g*obj.l;
            potGradLin = [K1, K2; K3, K4];
        end
        
        function inputMatLin = G(obj)
            % computes and returns the inputs mapping matrix of the
            % linearised cart-pole system
            inputMatLin = [1; 0];
        end
        
        % further methods
        function A_lin = A(obj)
            % compute and return the A matrix of the linearised cart-pole
            % system
            A1 = zeros(2, 2);
            A2 = eye(2);
            A3 = -(obj.M()\obj.K());    % -(M^-1)K
            A4 = -(obj.M()\obj.D());    % -(M^-1)D
            A_lin = [A1, A2; A3, A4];
        end
        
        function B_lin = B(obj)
            % compute and return the B matrix of the linearised cart-pole
            % system
            B1 = zeros(2, 1);
            B2 = obj.M()\obj.G();    % (M^-1)G
            B_lin = [B1; B2];
        end
        
        function dx = state_derivative(obj, x, F)
            % compute and return the time derivative of the state vector according to the linearised model
            dx = obj.A()*x + obj.B()*F;
        end
        
    end
    
    
    

end