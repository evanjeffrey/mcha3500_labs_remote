% robot_motor_sysid_mech
close all;
clearvars;
clc;

% load data
load('robot_motor2_mechData_12V_2Hz')

%% wrond data!!!!!!!!!!!
%%

% unpack data
time = motor1Data.time;
current = motor1Data.current;
velocity = -motor1Data.velocity; % flip this

% plot data to see what we are dealing with
figure(1)
subplot(2, 1, 1)
plot(time, current, 'b+')
xlabel('Time [s]')
ylabel('Current')
grid on
grid minor

subplot(2, 1, 2)
plot(time, velocity, 'b+')
xlabel('Time [s]')
ylabel('Velocity')
grid on
grid minor
% 
% % must define current as continuous
% % to find the value of 'current' interpolated between data points 'time' at t
I_a = @(t) interp1(time, current, t);

% give an initial guess at the parameters [Jw0; bf0; br0]
params_0 = [0.000141372141298000; 0.000154086474353612;  3.61068674408288e-05];

% % run simulation
y = run_sim(time, params_0, I_a);

% plot the results of the simulation
figure(1)
hold on
subplot(2, 1, 2)
plot(time, y, 'r')
legend("Measured", "Initial guess")

% define the measured points
y_measured = velocity;

% define the cost to be a function of the parameters
J = @(params) cost(y_measured, run_sim(time, params, I_a));

% run optimisation routine
params_opt = fminsearch(J, params_0, optimset('Display', 'iter'));

% now re-run simulation with the optimal parameters
y_opt = run_sim(time, params_opt, I_a);

velocity_hat = y_opt;

% plot with the optimal results
figure(1)
hold on
subplot(2, 1, 2)
plot(time, velocity_hat, 'k', 'LineWidth', 2)
legend('measured', 'guess sim', 'optimal sim')

%% create cost function to measure closeness of measurements to simulation
function J = cost(y_measured, y_simulated)
    J = norm(y_measured - y_simulated);
end


%% create a function that runs the simulation
%
function y = run_sim(sim_time, params, I_a)
    % unpack parameters
    Jw = params(1);
    
    % define gear ratio
    N = 20.4086;
    
    % define K_I
    K_I = 0.0051;
    
    % define state space equations
    dL = @(I_a, L) (N*K_I*I_a - friction_function(params, L));

    % ode wrapper
    dx = @(t, x) dL(I_a(t), x);

    % define initial conditions
    i_c = 0;
    
    % run the simulation using ode45
    [res.t, res.x] = ode45(dx, sim_time, i_c, odeset('RelTol', 1e-6));
    
    % transform the state of momentum into velocity
    res.velocity = N*(res.x/Jw);
    
    % pack return variable
    y = res.velocity;
    
end
% 
function friction_force = friction_function(params, L)
    % unpack parameters
    Jw = params(1);
    bf = params(2);
    br = params(3);
    
    % calculate velocity
    vel = (1/Jw)*L;
    
%     compute the conditional friction
    if vel >= 0
        friction_force = bf*vel;
    else
        friction_force = br*vel;
    end
end
















