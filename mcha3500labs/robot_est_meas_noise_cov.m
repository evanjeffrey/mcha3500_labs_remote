% robot_est_meas_noise_cov
close all;
clearvars;
clc;

%% estimate the sample mean and variance of the gyroscope readings

% load data
still_Data = load('robot_imuData_still');

% unpack data
time_still = still_Data.imuData.time;
accAngle_still = still_Data.imuData.imuAngle;
gyroVelocity_still = still_Data.imuData.imuVelocity;
encAngle_still = still_Data.imuData.encAngle;
N_still = length(time_still);   % number of data samples

% plot just to make sure
figure(1)

subplot(3, 1, 1)
plot(time_still, accAngle_still, '+')
xlabel("Time")
ylabel("IMU angle")
grid on
grid minor
grid on

subplot(3, 1, 2)
plot(time_still, gyroVelocity_still, '+')
xlabel("Time")
ylabel("IMU velocity")
grid on
grid minor
grid on

subplot(3, 1, 3)
plot(time_still, encAngle_still, '+')
xlabel("Time")
ylabel("Encoder angle")
grid on
grid minor
grid on

% sample mean
sample_mean_still = (1/N_still)*sum(gyroVelocity_still);

% sample covariance (using Bessel's correction)
sample_covariance_still = (1/(N_still-1))*((gyroVelocity_still - sample_mean_still).')*(gyroVelocity_still - sample_mean_still);

%% estimate the sample mean and variance of the angle (acc) readings
% load data
mov1_Data = load("robot_imuData_moving1");

% unpack data
time_mov1 = mov1_Data.imuData.time;
accAngle_mov1 = mov1_Data.imuData.imuAngle;
gyroVelocity_mov1 = mov1_Data.imuData.imuVelocity;
encAngle_mov1 = mov1_Data.imuData.encAngle;
N_mov1 = length(time_mov1);

% correct angle offset
enc_angle_init = mean(encAngle_mov1(1:200));
acc_angle_init = mean(accAngle_mov1(1:200));
encAngle_mov1 = encAngle_mov1 - enc_angle_init + acc_angle_init;
angle_noise = accAngle_mov1 - encAngle_mov1;

% compute sample mean
sample_mean_mov1 = (1/N_mov1)*sum(angle_noise);

% compute sample covariance, using Bessel's correction
sample_covariance_mov1 = (1/(N_mov1-1))*((angle_noise - sample_mean_mov1).')*(angle_noise - sample_mean_mov1);

%% Pack both sample covariances into the diagonal measurement noise covariance matrix and store this
% this is the measurement noise covariance, characteristic of the device,
% can be used again later (actually need to re-calibrate for the robot)
R = [sample_covariance_mov1, 0; 0, sample_covariance_still];

