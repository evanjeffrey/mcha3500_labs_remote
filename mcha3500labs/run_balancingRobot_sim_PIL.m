%%% Name: Evan Potts
close all;
clearvars;
clc;

%% Define STM serial device
% remove any persistant serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end
% Create and connect to device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);
% Reset STM
txStr = compose("reset");
fprintf(stm_device, txStr);
rxStr = fgets(stm_device);
rxStr = fgets(stm_device);

%% create balancing robot object
balancingRobot = classBalancingRobot;

%% define the plant parameters
balancingRobot.mc = 0.7327;             % robot: 0.7327 [kg]
balancingRobot.Jc = 0.1;                  % for robot, assume equal to 0 and lump into length; have a point mass
balancingRobot.mw = 0.5;                % robot: yet to measure
balancingRobot.Jw = 1.4e-4;             % robot: need to finish sysID
balancingRobot.r = 0.04025;             % robot: 0.04025 [m]
balancingRobot.l = 0.05;                 % robot: 
balancingRobot.g = 9.8;                 % accel due to gravity
balancingRobot.b_phi = 0.3;            % for now (need a different one for forwards and backwards)
balancingRobot.b_theta = 0.1;           % robot: 

%% create linearised balancing robot object
balancingRobotLinearised = classBalancingRobotLinear;

%% define the plant parameters
balancingRobotLinearised.mc = 0.7327;           % robot: 
balancingRobotLinearised.Jc = 0.1;                % robot: 
balancingRobotLinearised.mw = 0.5;              % robot: 
balancingRobotLinearised.Jw = 1.4e-4;           % robot: 
balancingRobotLinearised.r = 0.04025;           % robot: 
balancingRobotLinearised.l = 0.05;              % robot: 
balancingRobotLinearised.g = 9.8;               % robot: 
balancingRobotLinearised.b_phi = 1e-4;          % robot: 
balancingRobotLinearised.b_theta = 0.1;         % robot: 

%% create balancing robot control object
controllerbalancingRobot = classDiscreteLQRcontrol;

% frequency of 200 [Hz]
T = 1/200;

% define reference matrices
% what do I want to track? or set? probably 
C_r = [1, 0, 0, 0];
D_r = 0;

% define reference setpoint
y_ref = 1;

% continuous time tuning gains
Q = diag([1 5 1 10 10]);
R = 0.1;
N = zeros(5, 1);

% fetch the linearised A and B matrices, A_lin and B_lin, from the
% classBalancingRobotLinear class
A_lin = balancingRobotLinearised.A();
B_lin = balancingRobotLinearised.B();

% initialise the controller
controllerbalancingRobot.controlInit_reference_integrator(A_lin, B_lin, C_r, D_r, Q, R, N, T, y_ref);
% now K is stored in the class properties

%
%% Run a numerical simulation on the balancing robot system
% input parameters
input.alpha = 5*(pi/180);     % angle of the incline plane

% inputs
% T = (balancingRobot.mc + balancingRobot.mw)*balancingRobot.g*balancingRobot.r*sin(alpha);   % input torque to the motors
% T = 0.1;
% initial conditions
sim.phi_0 = 0;
sim.theta_0 = 3*(pi/180);
sim.dphi_0 = 0;
sim.dtheta_0 = 0;
sim.i_c = [sim.phi_0; sim.theta_0; sim.dphi_0; sim.dtheta_0];

% simulation time
sim.time = 0:T:10;   % seconds

% relative error tolerance of the solver
options = odeset('RelTol', 1e-6);

% set the start of the simulation results in the results vector
res.t = sim.time(1);
res.x = (sim.i_c).';

for i = 1:(length(sim.time) - 1)
    % define ode
    % define initial conditions for next solve
    
    i_c = res.x(end, :).';
    
    % Get control action from STM32
%     command_str = strcat("getControl", num2str(i_c(1)), " ", num2str(i_c(2)), " ", num2str(i_c(3)), " ", num2str(i_c(4)));
    % Catch any print return
    command_str = strcat("getControl ",num2str(i_c(1))," ",num2str(i_c(2))," ",num2str(i_c(3))," ",num2str(i_c(4)));
    txStr = compose(command_str);
    fprintf(stm_device, txStr);
    % Read return control action
    rxStr = fgets(stm_device);
    T_in = str2double(rxStr);
    
    sim.ode = @(t, x) balancingRobot.state_derivative(x, T_in, input.alpha);
    
    % run with ode45
    [t, x] = ode45(sim.ode, [sim.time(i) sim.time(i+1)], i_c, options);
    
    % concatinate solutions
    res.t = [res.t; t];     % just add more t's onto the column vector with each timestep
    res.x = [res.x; x];     % same here
end

%% Teardown STM32 connection
fclose(stm_device);
delete(stm_device);
clear stm_device % Close serial connection and clean up

%% Plot results
figure(1)

subplot(4, 1, 1)
plot(res.t, res.x(:, 1), 'b', 'LineWidth', 2)
title('Two-wheeled balancing robot with input torque')
xlabel('Time [s]')
ylabel('\phi [radians]')
grid on
grid minor

subplot(4, 1, 2)
plot(res.t, res.x(:, 2), 'b', 'LineWidth', 2)
xlabel('Time [s]')
ylabel('\theta [radians]')
grid on
grid minor

subplot(4, 1, 3)
plot(res.t, res.x(:, 3), 'b', 'LineWidth', 2)
xlabel('Time [s]')
ylabel('d\phi [radians/s]')
grid on
grid minor

subplot(4, 1, 4)
plot(res.t, res.x(:, 4), 'b', 'LineWidth', 2)
xlabel('Time [s]')
ylabel('d\theta [radians/s]')
grid on
grid minor
