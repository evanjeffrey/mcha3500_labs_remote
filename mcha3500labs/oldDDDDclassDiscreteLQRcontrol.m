% a handle class, classDiscreteLQRcontrol

classdef classDiscreteLQRcontrol < handle
   
    properties
    controlMode             % indicator of active control mode
    T                       % sampling period of controller
    A                       % linearised A matrix for controller
    B                       % linearised B matrix for controller
    C_r                     % linearised C matrix for regulation output
    D_r                     % linearised D matrix for regulation output
    A_int                   % A matrix augmented with integrator
    B_int                   % B matrix augmented with integrator
    Q                       % Q matrix for LQR cost
    R                       % R matrix for LQR cost
    N                       % N matrix for LQR cost
    K                       % optimal control gain
    N_x                     % feed-forward gain for states
    N_u                     % feed-forward gain for inputs
    y_ref                   % output regulation reference
    A_z                     % state coefficient for integrator transition dynamics
    B_z                     % input coefficient for integrator transition dynamics
    z                       % integrator state
    end
    
    methods
        
        % function to compute the optimal LQR state-feedback gain
        function [] = controlInit_regulation(obj, A, B, Q, R, N, T)
            % update the values of properties with the input values and
            % compute the discrete time optimal control gain using the
            % provided inputs
            obj.A = A;
            obj.B = B;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            
            % store the control gain in the property K
            obj.K = lqrd(obj.A, obj.B, obj.Q, obj.R, obj.N, obj.T);
            
            % update the propery controlMode with the string 'Regulation'
            obj.controlMode = 'Regulation';     % as in LQRegulation
        end
        
        % function to compute the control action based on the selected
        % control mode
        function u = control(obj, x)
            % compute the control input based on the provided states
            
            % switch between control modes
            switch obj.controlMode
                % regulation only control
                case 'Regulation'
                    u = -obj.K*x;
                    
                case 'Integrator'
                    % concatinate state vector with integrator state
                    state = [x; obj.z];
                    
                    % compute control signal
                    u = -obj.K*state;
                    
                    % update integrator state for the next iteration
                    obj.z = obj.z + obj.T*obj.C_r*x + obj.T*obj.D_r*u;
                    
                % default case if control is un-initialised
                otherwise
                    u = 0;
            end
        end
        
        % disturbance rejection
        function [] = controlInit_integrator(obj, A, B, C_r, D_r, Q, R, N, T)
            % compute the optimal LQR state-feedback gain and integrator
            % dynamics for a specified regulation output
            
            % store properties
            obj.A = A;
            obj.B = B;
            obj.C_r = C_r;
            obj.D_r = D_r;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            
            % construct continuous time augmented A and B matrices with
            % integrator
            obj.A_int = [obj.A, zeros(4, 1); obj.C_r, 0];
            obj.B_int = [obj.B; obj.D_r];
            
            % define the discrete time integrator update matrices using an
            % Euler discretisation, i.e. matrices that satisfy:
            % z_(k+1) = A_z*[x_k - x_star] + B_z*u_k
%             [Ad, Bd] = c2d(obj.A, obj.B, obj.T);
%             obj.A_z = [Ad, zeros(4, 1); obj.T*obj.C_r, eye(1)];
%             obj.B_z = [Bd; obj.T*obj.D_r];
            
            [obj.A_z, obj.B_z] = c2d(obj.A_int, obj.B_int, obj.T);
            
            % compute optimal control gain (from the discrete time plant model)
            obj.K = dlqr(obj.A_z, obj.B_z, obj.Q, obj.R, obj.N);
            
            % initialise integrator
            obj.z = 0;
            
            % update indicator
            obj.controlMode = 'Integrator';
            
        end
        
    end
    
end




