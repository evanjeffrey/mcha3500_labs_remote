% class classCartPole

% states are assumed to be:
% x = [d; dq]
% q = [s; theta]
% dq = v = [ds; dtheta]
% x = [s; theta; ds; dtheta]

%% define the class
classdef classCartPole
    %% define the parameters in properties
    properties
        mc          % mass of the cart
        mp          % mass of the pendulum
        l           % length of the pendulum arm
        r           % radius of the wheels of the cart
        g           % acceleration due to gravity
        b_s          % coefficient of friction of the cart travelling in the s coordinate
        b_theta      % coefficient of friction of the pendulum arm in the theta coordinate
    end
    
    %% create the methods (functions) associated with this instance of the class
    methods
        %% calc and return the mass matrix of the cart-pole system
        function massMat = M(obj, x)
            % unpack states from the full state vector
            q = x(1:2);
            theta = q(2);
            massMat = [(obj.mc + obj.mp), (obj.l*obj.mp*cos(theta)); (obj.l*obj.mp*cos(theta)), ((obj.l^2)*obj.mp)];
        end
        
        %% calc and return the Centripetal-Coriolis matrix of the cart-pole system
        function corMat = C(obj, x)
            % unpack states from the full state vector
            q = x(1:2);
            dq = x(3:4);
            theta = q(2);
            dtheta = dq(2);
            corMat = [0, -obj.l*obj.mp*dtheta*sin(theta); 0, 0];
        end
        
        %% calc and return the gradient of the potential energy wrt the configuration vector
        function potentialGrad = grav(obj, x, alpha)
            % unpack states from the full state vector
            q = x(1:2);
            theta = q(2);
            potentialGrad = [obj.g*(obj.mc + obj.mp)*sin(alpha); -obj.mp*obj.g*obj.l*sin(theta - alpha)];
        end
        
        %% calc and return the damping matrix of the cart-pole system
        function dampingMat = D(obj)
            dampingMat = [obj.b_s, 0; 0, obj.b_theta];
        end
        
        %% further methods
        %% calc and return the kinetic co-energy of the cart-pole system
        function kineticCoEng = T(obj, x)
            % unpack states from the full state vector
            q = x(1:2);
            dq = x(3:4);
            kineticCoEng = (1/2)*(dq')*obj.M(q)*dq;
        end
        
        %% calc and return the potential energy of the cart-pole system
        function potentialEng = V(obj, x, alpha)
            % unpack states from the full state vector
            q = x(1:2);
            s = q(1);
            theta = q(2);
            potentialEng = s*obj.g*sin(alpha)*(obj.mc + obj.mp) + obj.mp*obj.g*obj.l*cos(theta - alpha);
        end
        
        %% calc and return the time derivative of the state vector
        function dx = state_derivative(obj, x, F, alpha)
            % unpack states from the full state vector
            q = x(1:2);
            dq = x(3:4);
            dv = (inv(obj.M(x)))*([F; 0] - (obj.C(x) + obj.D())*dq - obj.grav(x, alpha));
            
            % concatenate the vectors dq and dv
            dx = [dq; dv];
        end
        
    end




end










