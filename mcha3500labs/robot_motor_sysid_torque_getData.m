%% robot_motor_sysid_torque_getData

close all;
clearvars;
clc;

%% collect data
% Define STM serial device
% remove any persistant serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end
% Create and connect to device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);
% Reset STM
% txStr = compose("reset");
% fprintf(stm_device, txStr);
% % Catch any print return
% rxStr = fgets(stm_device);
    
% run the sinusoidal voltage on the STM
command_str = strcat("sinusoid2");
txStr = compose(command_str);
fprintf(stm_device, txStr);

% log time, current and torque from the motor in question
txStr = compose("logCurrents");
fprintf(stm_device, txStr);

% define expected number of samples
N = 2000;

% initialise data structure for data
motorData.time = zeros(N,1);
motorData.current = zeros(N,1);
motorData.torqueV = zeros(N,1);        % define voltage in advance (hard coded into C at the moment)

% Collect the expected number of samples
for i=1:N
    % Read in data from serial connection
    rxStr = fgets(stm_device);
    
    % Separate string into parts
    separatedString = split(rxStr,',');
    
    % Convert data to numbers and store in data structure
    motorData.time(i) = str2double(separatedString{1});
    motorData.current(i) = str2double(separatedString{2});
    motorData.torqueV(i) = str2double(separatedString{3});
end

% Teardown
fclose(stm_device);
delete(stm_device);
clear stm_device % Close serial connection and clean up

%% conversion from voltage to torque

% constant voltage offset
torqueVoltageOffset = 1.6696;   % with motor 1
% torqueVoltageOffset = 1.7015;   % with motor 2

Rg = 50;
G = 1 + (19.8e3 / Rg);
sensorVpV = 1.352e-3;       % max scale 3 Nm = 1.352 mV
motorData.torque = -(sensorVpV/3 * 5 * G) * (motorData.torqueV - torqueVoltageOffset); % convert from V to Nm and fix sign error

%% Plot results
figure(1)
hold on

subplot(2, 1, 1)        % current
plot(motorData.time, motorData.current, '+')
xlabel("Time [s]")
ylabel("Current [A]")
grid on
grid minor

subplot(2, 1, 2)        % torque
plot(motorData.time, motorData.torque, '+')
xlabel("Time [s]")
ylabel("Torque [Nm]")
grid on
grid minor
















