// robot_encoders.h

// the following code protects against multiple declarations
#ifndef ROBOT_ENCODERS_H
#define ROBOT_ENCODERS_H

#define PI 3.141592653589793
#define GEAR_RATIO 20.4086
#define COUNT_RES 48.0

/* add function prototypes here */
void motor1_encoder_init(void);
void motor2_encoder_init(void);
// void swingRig_encoder_init(void);
int32_t motor1_encoder_getValue(void);
int32_t motor2_encoder_getValue(void);
// int32_t swingRig_encoder_getValue(void);

#endif






























