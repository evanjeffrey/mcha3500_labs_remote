// motor.c

// add dependencies
// #include <stdint.h>
// #include <stdlib.h>
// #include <math.h>
// #include "stm32f4xx_hal.h"
// #include "cmsis_os2.h"
// #include "uart.h"
// #include "motor.h"

// declare function handles
// static TIM_HandleTypeDef htim3;
// static TIM_OC_InitTypeDef sConfigPWM;

// static uint8_t _is_init = 0;		/* set the initialisation variable so that motor_PWM_init() will only run once */
// static uint8_t _is_init2 = 0;		/* set the initialisation variable so that motor_encoder_init() will only run once */
// int32_t enc_count;				/* declare the encoder count variable and start it at 0 */

// initialise PA6 (Timer 3, Channel 1) as a PWM output
// void motor_PWM_init(void)
// {
	// if(!_is_init)
	// {	
		// enable clock for GPIOA
		// __TIM3_CLK_ENABLE();
		// __GPIOA_CLK_ENABLE();

		// initialise pin 6
		// GPIO_InitTypeDef  GPIO_InitStructure;
		// GPIO_InitStructure.Pin = GPIO_PIN_6;				/* pin 6 */
		// GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;			/* alternate function push-pull mode */
		// GPIO_InitStructure.Pull = GPIO_NOPULL;				/* no pull */
		// GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;	/* high frequency */
		// GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;		/* alternate function 2 - timer 3 */
		// HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);			/* initialise PA6 */

		// htim3.Instance = TIM3;								/* instance TIM3 */
		// htim3.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1;		/* prescalar of 1 */
		// htim3.Init.CounterMode = TIM_COUNTERMODE_UP;		/* counter mode up */
		// htim3.Init.Period = 10000;							/* timer period to generate a 10kHz signal */
		// htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;	/* clock division of 0 */
		// HAL_TIM_PWM_Init(&htim3);							/* initialise timer 3 */	
		
		// sConfigPWM.OCMode = TIM_OCMODE_PWM1;							/* output compare mode PWM1 */
		// sConfigPWM.Pulse = 0;											/* pulse = 0 */
		// sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;					/* OC polarity high */	
		// sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;						/* fast mode disabled */
		// HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigPWM, TIM_CHANNEL_1);	/* configure timer 3, channel 1 */
			
		// __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 2500);	/* set initial Timer 3, channel 1 compare value */
		
		// HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);			/* start Timer 3, Channel 1 */
		
		// _is_init = 1;				/* set so this function will not run again */
	// }
// }

// create a function which initialises the pins PC0 and PC1 as interrupts on both a rising and falling edge
// void motor_encoder_init(void)
// {
	// if(!_is_init2)
	// {	
		// __HAL_RCC_GPIOC_CLK_ENABLE();								/* enable GPIOC clock */

		// GPIO_InitTypeDef  GPIO_InitStructure2;
		// GPIO_InitStructure2.Pin = GPIO_PIN_0|GPIO_PIN_1;			/* pins 0 and 1 */
		// GPIO_InitStructure2.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
		// GPIO_InitStructure2.Pull = GPIO_NOPULL;						/* no pull */
		// GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
		// HAL_GPIO_Init(GPIOC, &GPIO_InitStructure2);					/* initialise PC0 and PC1 */
			
		// HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
		// HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
		
		// HAL_NVIC_EnableIRQ(EXTI0_IRQn);								/* enable external interrupt for lines 0 and 1 */	
		// HAL_NVIC_EnableIRQ(EXTI1_IRQn);								/* enable external interrupt for lines 0 and 1 */
		
		// _is_init2 = 1;
	// }
// }

// IQR handler for PC0
// void EXTI0_IRQHandler(void)
// {
	// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
	// {
		// enc_count = enc_count+1;							/* up the encoder by 1 count */
	// }
	// else
	// {
		// enc_count = enc_count-1;							/* decrease the encoder by 1 count */
	// }

	// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);					/* reset interrupt */
// }

// IQR handler for PC1
// void EXTI1_IRQHandler(void)
// {
	// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
	// {
		// enc_count = enc_count-1;							/* up the encoder by 1 count */
	// }
	// else
	// {
		// enc_count = enc_count+1;							/* decrease the encoder by 1 count */
	// }

	// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);					/* reset interrupt */
// }

// write a function that returns the value of the encoder count when called
// int32_t motor_encoder_getValue(void)
// {
	// return enc_count;
// }

