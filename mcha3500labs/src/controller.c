// pendulum.c

#define CTRL_N_INPUT 1
#define CTRL_N_STATE 5

// add dependencies here
#include <stddef.h>
#include "stm32f4xx_hal.h"			// includes all of the HAL functions	
#include "arm_math.h"				// includes STM32 DSP matrix libraries
#include "controller.h"				// allows you to call other functions defined in controller.c

/* define matrix control variables */
static float ctrl_mK_f32[CTRL_N_INPUT*CTRL_N_STATE] =
{
	/* negative K (1x5) */
	57.5188, 161.9404, 49.4646, 26.5821, 29.8305,
};

static float ctrl_x_f32[CTRL_N_STATE] =
{
	/* estimate of state, (5x1) */
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};

static float ctrl_u_f32[CTRL_N_INPUT] =
{
	/* control action, (1x1) */
	0.0,
};

static float ctrl_Az_f32[CTRL_N_STATE] =
{
	/* state transition matrix (1x5? why not the 5x5?) */
	/* COME BACK AND FIX THIS...NOT SURE WHAT THIS IS MEANT TO BE!!!!!!!!!!!!! */
	0.0050,   0.0000,    0.0000,    0.0000,     0,
};

static float ctrl_z_f32[CTRL_N_INPUT] =
{
	/* Integrator state */
	0.0,
};

/* define control matrix variables */
/* defined as rows, columns, data arrays */
arm_matrix_instance_f32 ctrl_mK = {CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32};
arm_matrix_instance_f32 ctrl_x = {CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32};
arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};
arm_matrix_instance_f32 ctrl_Az = {1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32};
arm_matrix_instance_f32 ctrl_z = {1, 1, (float32_t *)ctrl_z_f32};

/* control functions */
void ctrl_init(void)
{
	arm_mat_init_f32(&ctrl_mK, CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32);
	arm_mat_init_f32(&ctrl_x, CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32);
	arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
	arm_mat_init_f32(&ctrl_Az, 1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32);
	arm_mat_init_f32(&ctrl_z, 1, 1, (float32_t *)ctrl_z_f32);
}

/* update state vector elements */
void ctrl_set_x1(float x1)
{
	/* update state x1 */
	ctrl_x_f32[0] = x1;
}

void ctrl_set_x2(float x2)
{
	/* update state x1 */
	ctrl_x_f32[1] = x2;
}

void ctrl_set_x3(float x3)
{
	/* update state x1 */
	ctrl_x_f32[2] = x3;
}

void ctrl_set_x4(float x4)
{
	/* update state x1 */
	ctrl_x_f32[3] = x4;
}

/* get the current control output */
float getControl(void)
{
	return ctrl_u_f32[0];
}

/* create a function which computes the control signal and then updates the state of the integrator */

/* update control output */
void ctrl_update(void)
{
	/* compute control action */
	arm_mat_mult_f32(&ctrl_mK, &ctrl_x, &ctrl_u);	/* -K*x = u */
	
	/* update integrator state */
	
	/* store the first z term */
	float z_orig = ctrl_z_f32[0];
	
	/* update to the new z term */
	arm_mat_mult_f32(&ctrl_Az, &ctrl_x, &ctrl_z); 	/* I think this is it ... just trialling at the moment */
	
	/* add the original and new z term together */
	ctrl_z_f32[0] = z_orig + ctrl_z_f32[0];
	
	/* copy updated value of integrator state into state vector */
	ctrl_x_f32[4] = ctrl_z_f32[0];
}































