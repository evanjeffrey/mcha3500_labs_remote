// pendulum.c

// add dependencies here
#include <stdint.h> 				//	includes standard integer types		 
#include <stdlib.h>					// defines several useful functions for converting between strings and numbers
#include <math.h>					// defines several useful math functions
#include "stm32f4xx_hal.h"			// includes all of the HAL functions	
#include "cmsis_os2.h"				// includes all of the RTOS functions
#include "uart.h"					// allows you to print to console using the printf command
#include "pendulum.h"				// allows you to call other functions defined in pendulum.c

// define data structures
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;

static uint8_t _is_init = 0;		/* for the initialisation function */

// create a function to initialise the hardware on the STM32
void pendulum_init(void)
{
	if (!_is_init)		/* since we only need to initialise the hardware once */
	{
		/* enable ADC1 clock */
		__HAL_RCC_ADC1_CLK_ENABLE();
		
		/* enable GPIOB clock */
		__HAL_RCC_GPIOB_CLK_ENABLE();
		
		GPIO_InitTypeDef  GPIO_InitStructure;
		GPIO_InitStructure.Pin = GPIO_PIN_1;					/* Pin 0 */
		GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;				/* analog mode */
		GPIO_InitStructure.Pull = GPIO_NOPULL;					/* no pull */
		GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;		/* high frequency */
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);				/* initialise PB0 */	
		
		
		HAL_ADC_Init(&hadc1);									/* initialise ADC1 */
		hadc1.Instance = ADC1;									/* instance ADC1 */
		hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;	/* div 2 prescaler */
		hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;				/* data align right */
		hadc1.Init.Resolution = ADC_RESOLUTION_12B;				/* 12 bit resolution */
		hadc1.Init.ContinuousConvMode = DISABLE;				/* continuous conversion mode disabled */
		hadc1.Init.NbrOfConversion = 1;							/* number of conversions = 1 */

		sConfigADC.Channel = ADC_CHANNEL_8;						/* channel 8 */
		sConfigADC.Rank = 1;									/* rank 1 */
		sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;		/* sampling time 480 cycles */
		sConfigADC.Offset = 0;									/* offset 0 */
		HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);				/* configure ADC channel */
	}
}

// create a function which reads the voltage of PB0 and returns the voltage as a float
float pendulum_read_voltage(void)
{
	HAL_ADC_Start(&hadc1);									/* start the ADC */
	HAL_ADC_PollForConversion(&hadc1, 0xFF);				/* poll for conversion; use timeout of 0xFF */
	uint16_t fetched_voltage = HAL_ADC_GetValue(&hadc1);	/* get the ADC value and stored it fetched_voltage */
	HAL_ADC_Stop(&hadc1);									/* stop the ADC */
	
	/* compute the voltage from the ADC. Hint: 2^12-1 = 4095 */
	float voltage = (((float)fetched_voltage)/4095.0)*3.3;
	
	/* return the computed voltage */
	return voltage;
}













































