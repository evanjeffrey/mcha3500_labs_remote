// data_logging.h

// the following code protects against multiple declarations
#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H

/* add function prototypes here */
void logging_init(void);
void pend_logging_start(void);
void logging_stop(void);
void imu_logging_start(void);
void encoder1_logging_start(void);
void encoder2_logging_start(void);
void currents_logging_start(void);
void all_logging_start(void);
void all_logging_motor1_start(void);
void all_logging_motor2_start(void);
void torque1_logging_start(float);
void torque2_logging_start(float);
void sinTorque1_logging_start(float, float);
void sinTorque2_logging_start(float, float);
void sinVoltage1_logging_start(float, float);
void sinVoltage2_logging_start(float, float);

#endif


















































