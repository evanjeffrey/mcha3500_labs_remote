// just start writing shit for the robot
// just start writing shit for the robot
// muck_around.c

/* apply voltages to motors */
/* call this function motor_robot.c */


/* includes */


	/* initialise pins (red and black power) as PWM outputs, these will be driven directly from the motor drivers */
	/* the pins will be motor MOTOR_PWM1 and MOTOR_PWM2 */
	/* MOTOR1_PWM1 PA6
	   MOTOR1_PWM2 PA7 
	   MOTOR1_EN PC5
	   MOTOR1_ENB PC6
	   MOTOR2_PWM1 PB8
	   MOTOR2_PWM2 PB9
	   MOTOR2_EN PB4
	   MOTOR2_ENB PB5
	   
	   
	   if (forward)
		   MOTOR1_PWM1 = HIGH (THE DUTY CYCLE);
	       MOTOR1_PWM2 = LOW (constant);
		   MOTOR1_EN = HIGH;
		   MOTOR1_ENB = LOW;
		   
		   MOTOR2_PWM1 = HIGH (THE DUTY CYCLE);
	       MOTOR2_PWM2 = LOW (constant);
		   MOTOR2_EN = HIGH;
		   MOTOR2_ENB = LOW;
	   end
	   
		if (brake after or during forward)
		   MOTOR1_PWM1 = LOW;
	       MOTOR1_PWM2 = LOW;
		   MOTOR1_EN = HIGH;
		   MOTOR1_ENB = LOW;
		   
		   MOTOR2_PWM1 = LOW (THE DUTY CYCLE);
	       MOTOR2_PWM2 = LOW (constant);
		   MOTOR2_EN = HIGH;
		   MOTOR2_ENB = LOW;
	   end
	   
	   if (reverse)
		   MOTOR1_PWM1 = LOW (THE DUTY CYCLE);
	       MOTOR1_PWM2 = HIGH (constant);
		   MOTOR1_EN = HIGH;
		   MOTOR1_ENB = LOW;
		   
		   MOTOR2_PWM1 = LOW (THE DUTY CYCLE);
	       MOTOR2_PWM2 = HIGH (constant);
		   MOTOR2_EN = HIGH;
		   MOTOR2_ENB = LOW;
	   end
	   
	   if (brake after or during reverse)
		   MOTOR1_PWM1 = HIGH (THE DUTY CYCLE);
	       MOTOR1_PWM2 = HIGH (constant);
		   MOTOR1_EN = HIGH;
		   MOTOR1_ENB = LOW;
		   
		   MOTOR2_PWM1 = HIGH (THE DUTY CYCLE);
	       MOTOR2_PWM2 = HIGH (constant);
		   MOTOR2_EN = HIGH;
		   MOTOR2_ENB = LOW;
	   end
	   */


/********************************************************************************************/
// MOTOR ENCODERS

	// ENCODER FOR MOTOR 1
		// void motor1_encoder_init(void)
		// {	
			// if(!_is_init_m1_enc) 											/* motor 1 encoder init 'tracker' */
			// {	
				// __HAL_RCC_GPIOC_CLK_ENABLE();								/* enable GPIOC clock */

				// GPIO_InitTypeDef  GPIO_InitStructure2;
				// GPIO_InitStructure2.Pin = GPIO_PIN_0|GPIO_PIN_1;			/* pins 0 and 1 */
				// GPIO_InitStructure2.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
				// GPIO_InitStructure2.Pull = GPIO_NOPULL;						/* no pull */
				// GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
				// HAL_GPIO_Init(GPIOC, &GPIO_InitStructure2);					/* initialise PC0 and PC1 */
					
				// HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
				// HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
				
				// HAL_NVIC_EnableIRQ(EXTI0_IRQn);								/* enable external interrupt for lines 0 and 1 */	
				// HAL_NVIC_EnableIRQ(EXTI1_IRQn);								/* enable external interrupt for lines 0 and 1 */
				
				// _is_init_m1_enc = 1;
			// }
		// }

		// IQR handler for PC0
		// void EXTI0_IRQHandler(void)
		// {
			// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
			// {
				// enc1_count = enc1_count+1;							/* up the encoder by 1 count */
			// }
			// else
			// {
				// enc1_count = enc1_count-1;							/* decrease the encoder by 1 count */
			// }

			// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);					/* reset interrupt */
		// }

		// IQR handler for PC1
		// void EXTI1_IRQHandler(void)
		// {
			// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
			// {
				// enc1_count = enc1_count-1;							/* up the encoder by 1 count */
			// }
			// else
			// {
				// enc1_count = enc1_count+1;							/* decrease the encoder by 1 count */
			// }

			// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);					/* reset interrupt */
		// }

		// write a function that returns the value of the encoder count when called
		// int32_t motor1_encoder_getValue(void)
		// {
			// return enc1_count;
		// }
	
	
	
	
	// ENCODER FOR MOTOR 2
		// void motor2_encoder_init(void)
		// {	
			// if(!_is_init_m2_enc) 											/* motor 2 encoder init 'tracker' */
			// {	
				// __HAL_RCC_GPIOC_CLK_ENABLE();								/* enable GPIOC clock */

				// GPIO_InitTypeDef  GPIO_InitStructure2;
				// GPIO_InitStructure2.Pin = GPIO_PIN_2|GPIO_PIN_3;			/* pins 2 and 3 */
				// GPIO_InitStructure2.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
				// GPIO_InitStructure2.Pull = GPIO_NOPULL;						/* no pull */
				// GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
				// HAL_GPIO_Init(GPIOC, &GPIO_InitStructure2);					/* initialise PC2 and PC3 */
					
				// HAL_NVIC_SetPriority(EXTI2_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
				// HAL_NVIC_SetPriority(EXTI3_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
				
				// HAL_NVIC_EnableIRQ(EXTI2_IRQn);								/* enable external interrupt for lines 2 and 3 */	
				// HAL_NVIC_EnableIRQ(EXTI3_IRQn);								/* enable external interrupt for lines 2 and 3 */
				
				// _is_init_m2_enc = 1;
			// }
		// }

		// IQR handler for PC0
		// void EXTI2_IRQHandler(void)
		// {
			// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))								/* check if PC2 == PC3 */
			// {
				// enc2_count = enc2_count+1;							/* up the encoder by 1 count */
			// }
			// else
			// {
				// enc2_count = enc2_count-1;							/* decrease the encoder by 1 count */
			// }

			// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);					/* reset interrupt */
		// }

		// IQR handler for PC1
		// void EXTI3_IRQHandler(void)
		// {
			// if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))								/* check if PC2 == PC3 */
			// {
				// enc2_count = enc2_count-1;							/* up the encoder by 1 count */
			// }
			// else
			// {
				// enc2_count = enc2_count+1;							/* decrease the encoder by 1 count */
			// }

			// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);					/* reset interrupt */
		// }

		// write a function that returns the value of the encoder count when called
		// int32_t motor2_encoder_getValue(void)
		// {
			// return enc2_count;
		// }






// ESTIMATE MOTOR ANGULAR VELOCITY
	/* 
		is this just (current_encoder_position - previous_encoder_position)/sample_time?
	 */










// MEASURE CURRENT THROUGH MOTORS

	// MOTOR 1
		/* current comes in at pin PB0 */
		/* initialise GPIOB,  */
		


	// MOTOR 2
		/* current comes in at pin PB1 */
		




















/* measure IMU angle about X axis using accelerations */



/* measure gyroscope angular velocity about X axis */

























