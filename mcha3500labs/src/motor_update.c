// motor_update.c

/* includes */
// #include <stdint.h>
// #include <stdlib.h>
// #include <math.h>
// #include "stm32f4xx_hal.h"
// #include "cmsis_os2.h"
// #include "uart.h"
// #include "drive_robot_motors.h"

/* defines */
// #ifndef M_PI
	// #define M_PI 3.14159265358979323846
// #endif

// float V; 			/* voltage to give to the motors */
// uint16_t direc; 	/* direction to drive the motors */

/* function declarations */
// static void motor_pointer(void *argument);
// static void drive_motor1(void);
// static void drive_motor2(void);

/* variable declarations */
// uint16_t motorCount;
// static void (*motor_function)(void);

/* timer declarations *
// static osTimerId_t _motorTimerID;
// static osTimerAttr_t motorTimAttr = {
    // .name = "motorTimer" 
// };

/* initialise the motor timer */
// void motor_init(void)
// {
	// /* initialise timer for use with pendulum data logging*/
	// _motorTimerID = osTimerNew(motor_pointer, osTimerPeriodic, NULL, &motorTimAttr);
// }

// static void motor_pointer(void *argument)
// {
	// UNUSED(argument);
	
	// /* call function pointed to by motor_function */
	// (*motor_function)();
// }

// void motor1_drive_stop(void)
// {
	// /* make sure timer was already running. If not, can stop the timer */
	// if (osTimerIsRunning(_motorTimerID))
	// {
		// /* start motor timer at 200 [Hz] */
		// osTimerStop(_motorTimerID);
	// }
// }

// void motor2_drive_stop(void)
// {
	// /* make sure timer was already running. If not, can stop the timer */
	// if (osTimerIsRunning(_motorTimerID))
	// {
		// /* start motor timer at 200 [Hz] */
		// osTimerStop(_motorTimerID);
	// }
// }

// void motor1_drive_start(void)
// {
	// /* change function pointer to the IMU logging function (drive_motor) */
	// motor_function = &drive_motor1;
	
	// /* reset the log counter */
	// motorCount = 0;
	
	// /* start data logging at 200 [Hz] (make sure timer was not already running first) */
	// if (!osTimerIsRunning(_motorTimerID))
	// {
		// /* start data logging timer at 200 [Hz] */
		// osTimerStart(_motorTimerID, 5);
	// }
// }

// void motor2_drive_start(void)
// {
	// /* change function pointer to the IMU logging function (drive_motor) */
	// motor_function = &drive_motor2;
	
	// /* reset the log counter */
	// motorCount = 0;
	
	// /* start data logging at 200 [Hz] (make sure timer was not already running first) */
	// if (!osTimerIsRunning(_motorTimerID))
	// {
		// /* start data logging timer at 200 [Hz] */
		// osTimerStart(_motorTimerID, 5);
	// }
// }

/* function to configure the sinusoid */
// void read_sinusoid_config1(float *drive_config1)
// {	
	// /* fill the values of the function */
	// drive_config1[0] = 6.0; 		/* amplitude in V */
	// drive_config1[1] = 0.5; 		/* frequency in Hz */
// }

// void read_sinusoid_config2(float *drive_config2)
// {	
	// /* fill the values of the function */
	// drive_config2[0] = 6.0; 		/* amplitude in V */
	// drive_config2[1] = 2.0; 		/* frequency in Hz */
// }

// static void drive_motor1(void)
// {
	/* set to motor 1 */
	// uint16_t motor_num = 1;
	
	// /* note the motor count here will start at 0 */
	
	// /* declare motor config array */
	// float drive_config1[2];
	
	// /* call the function to fill the values */
	// read_sinusoid_config1(drive_config1);
	
	// /*assign the values of the array to new variables*/
	// float amplitude = drive_config1[0];
	// float frequency = drive_config1[1];
	
	
	// /* calculate the voltage needed for the current sine wave */
	/* V = 5*sin(M_PI*(1.0)*((float)motorCount/200.0)); */ 		/* ((float)motorCount/200) is the time in seconds */
	// V = amplitude*sin(frequency*2.0*M_PI*((float)motorCount/200.0));
	
	// /* determine the direction of rotation of the motor */
	// /* this direction will be based on the direction of the sine wave. If pos, forward, if neg, reverse */
	/* if(V >= 0)
	{
		direc = 4; 	/* forward direction flag */
	// }
	// else
	// {
		// V = -V; 	/* the input to the duty cycle needs to be a positive number */
		// direc = 5;  /* reverse direction flag */
	// }
	
	// /* push this calculated voltage through to the set_voltage function within the drive_robot_motors() module */
	// set_motor1_voltage(V);
	
	// /* increment log count */
	// motorCount = motorCount + 1;
	
	// /* set the de-initial value so the iterative process will start */
	// _is_init_1 = 1;
	
	/* stop driving once 5 seconds is reached */
	// if(motorCount==2000)
	// {
		// V = 0.0;
		// motor1_drive_stop();		/* call the function to stop the logging */
		// direc = 1; 				/* set the direction to be the brake flag */
		// set_motor1_voltage(V);
	// }
	
	// print the motor count
	// printf("%d %f\n", motorCount, V);
// }

// static void drive_motor2(void)
// {
	// set to motor 1
	// uint16_t motor_num = 2;
	
	// /* declare motor config array */
	// float drive_config2[2];
	
	// /* call the function to fill the values */
	// read_sinusoid_config2(drive_config2);
	
	// /*assign the values of the array to new variables*/
	// float amplitude = drive_config2[0];
	// float frequency = drive_config2[1];
	
	
	// /* calculate the voltage needed for the current sine wave */
	// V = amplitude*sin(frequency*2.0*M_PI*((float)motorCount/200.0));
	
	// /* determine the direction of rotation of the motor */
	// if(V >= 0)
	// {
		// direc = 4; 	/* forward direction flag */
	// }
	// else
	// {
		// V = -V; 	/* the input to the duty cycle needs to be a positive number */
		// direc = 5;  /* reverse direction flag */
	// }
	
	// /* push this calculated voltage through to the set_voltage function within the drive_robot_motors() module */
	// set_motor2_voltage(V);
	
	// /* increment log count */
	// motorCount = motorCount + 1;
	
	// if(motorCount==2000)
	// {
		// V = 0.0;
		// motor2_drive_stop();		/* call the function to stop the logging */
		// direc = 1; 				/* set the direction to be the brake flag */
		// set_motor2_voltage(V);
	// }
// }






















