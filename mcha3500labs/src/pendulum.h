// pendulum.h

// the following code protects against multiple declarations
#ifndef PENDULUM_H
#define PENDULUM_H

/* add function prototypes here */
void pendulum_init(void);			/* hardware initialisation function */

float pendulum_read_voltage(void);	/* voltage reader function */



#endif






