/********************************************************************************************/
// robot_encoders.c
// MOTOR ENCODERS

// includes
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "uart.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "robot_encoders.h"

// defines
static uint8_t _is_init_m1_enc = 0;
static uint8_t _is_init_m2_enc = 0;
static uint8_t _is_init_swing_enc = 0;
int32_t enc1_count;
int32_t enc2_count;
int32_t swing_count;


// ENCODER FOR MOTOR 1
void motor1_encoder_init(void)
{	
	if(!_is_init_m1_enc) 											/* motor 1 encoder init 'tracker' */
	{	
		__HAL_RCC_GPIOC_CLK_ENABLE();								/* enable GPIOC clock */

		GPIO_InitTypeDef  GPIO_InitStructure2;
		GPIO_InitStructure2.Pin = GPIO_PIN_0|GPIO_PIN_1;			/* pins 0 and 1 */
		GPIO_InitStructure2.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
		GPIO_InitStructure2.Pull = GPIO_NOPULL;						/* no pull */
		GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
		HAL_GPIO_Init(GPIOC, &GPIO_InitStructure2);					/* initialise PC0 and PC1 */
			
		HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
		HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 0, 1 to 0x0f, 0x0f*/
		
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);								/* enable external interrupt for lines 0 and 1 */	
		HAL_NVIC_EnableIRQ(EXTI1_IRQn);								/* enable external interrupt for lines 0 and 1 */
		
		_is_init_m1_enc = 1;
	}
}

// IQR handler for PC0
void EXTI0_IRQHandler(void)
{
	if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
	{
		enc1_count = enc1_count+1;							/* up the encoder by 1 count */
	}
	else
	{
		enc1_count = enc1_count-1;							/* decrease the encoder by 1 count */
	}

	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);					/* reset interrupt */
}

// IQR handler for PC1
void EXTI1_IRQHandler(void)
{
	if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))								/* check if PC0 == PC1 */
	{
		enc1_count = enc1_count-1;							/* up the encoder by 1 count */
	}
	else
	{
		enc1_count = enc1_count+1;							/* decrease the encoder by 1 count */
	}

	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);					/* reset interrupt */
}

// write a function that returns the value of the encoder count when called
int32_t motor1_encoder_getValue(void)
{
	// start writing a function that computes the angle of the shaft
	/* based on the counts/rev, the encoder count and the gear ratio */
	/* 
		do I need to implement some sort of incremental algorithm in order to track the change in encoder count rather than the absolute position? 
		
		phi1 = enc1_count*(1/GEAR_RATIO)*COUNT_RES*2*PI;
		sample_time = 1/200; 					// sample frequency is 200 [Hz]
		dphi1 = phi1/sample_time; 				// maybe the sample time needs to come from somewhere else defined? Make sure it matches up with the data logging functions!!!
	*/

	return enc1_count;
}


// ENCODER FOR MOTOR 2
void motor2_encoder_init(void)
{	
	if(!_is_init_m2_enc) 											/* motor 2 encoder init 'tracker' */
	{	
		__HAL_RCC_GPIOC_CLK_ENABLE();								/* enable GPIOC clock */

		GPIO_InitTypeDef  GPIO_InitStructure2;
		GPIO_InitStructure2.Pin = GPIO_PIN_2|GPIO_PIN_3;			/* pins 2 and 3 */
		GPIO_InitStructure2.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
		GPIO_InitStructure2.Pull = GPIO_NOPULL;						/* no pull */
		GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
		HAL_GPIO_Init(GPIOC, &GPIO_InitStructure2);					/* initialise PC2 and PC3 */
			
		HAL_NVIC_SetPriority(EXTI2_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
		HAL_NVIC_SetPriority(EXTI3_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
		
		HAL_NVIC_EnableIRQ(EXTI2_IRQn);								/* enable external interrupt for lines 2 and 3 */	
		HAL_NVIC_EnableIRQ(EXTI3_IRQn);								/* enable external interrupt for lines 2 and 3 */
		
		_is_init_m2_enc = 1;
	}
}

/* IQR handler for PC2 */
void EXTI2_IRQHandler(void)
{
	if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))								/* check if PC2 == PC3 */
	{
		enc2_count = enc2_count+1;							/* up the encoder by 1 count */
	}
	else
	{
		enc2_count = enc2_count-1;							/* decrease the encoder by 1 count */
	}

	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);					/* reset interrupt */
}

/* IQR handler for PC3 */
void EXTI3_IRQHandler(void)
{
	if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)==HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))								/* check if PC2 == PC3 */
	{
		enc2_count = enc2_count-1;							/* up the encoder by 1 count */
	}
	else
	{
		enc2_count = enc2_count+1;							/* decrease the encoder by 1 count */
	}

	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);					/* reset interrupt */
}

// write a function that returns the value of the encoder count when called
int32_t motor2_encoder_getValue(void)
{
	return enc2_count;
}
	 
	 
// ENCODER FOR SWING RIG
// void swingRig_encoder_init(void)
// {	
	// /* using PA11 and PA12 */
	// if(!_is_init_swing_enc) 											/* swing rig encoder init 'tracker' */
	// {	
		// /* use PA4 and PB2 */
	
		// __HAL_RCC_GPIOA_CLK_ENABLE();								/* enable GPIOA clock */
		// __HAL_RCC_GPIOB_CLK_ENABLE();								/* enable GPIOA clock */
		

		// GPIO_InitTypeDef  GPIO_InitStructure3;
		// GPIO_InitStructure3.Pin = GPIO_PIN_4;						/* pin PA4 */
		// GPIO_InitStructure3.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
		// GPIO_InitStructure3.Pull = GPIO_NOPULL;						/* no pull */
		// GPIO_InitStructure3.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
		// HAL_GPIO_Init(GPIOA, &GPIO_InitStructure3);					/* initialise PA4 */
		
		// GPIO_InitTypeDef  GPIO_InitStructure4;
		// GPIO_InitStructure4.Pin = GPIO_PIN_2;						/* pin PB2 */
		// GPIO_InitStructure4.Mode = GPIO_MODE_IT_RISING_FALLING;		/* interrupt rising and falling edge */
		// GPIO_InitStructure4.Pull = GPIO_NOPULL;						/* no pull */
		// GPIO_InitStructure4.Speed = GPIO_SPEED_FREQ_HIGH;			/* high frequency */
		// HAL_GPIO_Init(GPIOB, &GPIO_InitStructure4);					/* initialise PA4 */
			
		// HAL_NVIC_SetPriority(EXTI4_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
		// HAL_NVIC_SetPriority(EXTI2_IRQn, 0x0f, 0x0f);				/* set priority of external interrupt lines 2, 3 to 0x0f, 0x0f*/
		
		// HAL_NVIC_EnableIRQ(EXTI4_IRQn);								/* enable external interrupt for lines 2 and 3 */	
		// HAL_NVIC_EnableIRQ(EXTI2_IRQn);								/* enable external interrupt for lines 2 and 3 */
		
		// _is_init_swing_enc = 1;
	// }
// }

// IQR handler for PA4
// void EXTI4_IRQHandler(void)
// {
	// if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4)==HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2))								/* check if PA4 == PB2 */
	// {
		// swing_count = swing_count+1;							/* up the encoder by 1 count */
	// }
	// else
	// {
		// swing_count = swing_count-1;							/* decrease the encoder by 1 count */
	// }

	// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);					/* reset interrupt */
// }

// IQR handler for PB2
// void EXTI2_IRQHandler(void)
// {
	// if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4)==HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2))								/* check if PA4 == PB2 */
	// {
		// swing_count = swing_count-1;							/* up the encoder by 1 count */
	// }
	// else
	// {
		// swing_count = swing_count+1;							/* decrease the encoder by 1 count */
	// }

	// HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);					/* reset interrupt */
// }

// write a function that returns the value of the encoder count when called
// int32_t swingRig_encoder_getValue(void)
// {
	// start writing a function that computes the angle of the shaft
	// /* based on the counts/rev, the encoder count and the gear ratio */
	// /* 
		// do I need to implement some sort of incremental algorithm in order to track the change in encoder count rather than the absolute position? 
		
		// phi1 = enc1_count*(1/GEAR_RATIO)*COUNT_RES*2*PI;
		// sample_time = 1/200; 					// sample frequency is 200 [Hz]
		// dphi1 = phi1/sample_time; 				// maybe the sample time needs to come from somewhere else defined? Make sure it matches up with the data logging functions!!!
	// */
	// swing_count = 5;

	// return swing_count;
// }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 