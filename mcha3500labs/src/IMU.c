// IMU.c

#include "math.h"
#include "tm_stm32_mpu6050.h"
#include "IMU.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

/* variable declarations */
TM_MPU6050_t IMU_datastruct;
float accY;
float accZ;
float gyroX;
double acc_angle;

/* function definitions */

// function to initialise the MPU6050
void IMU_init(void)
{
	/* initialise IMU with ADO LOW, acceleration sensitivity +- 4 [g], gyroscope +- 250 [deg/s] */
	TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G , TM_MPU6050_Gyroscope_250s);
}

// function to read all values from the MPU6050
void IMU_read(void)
{
	TM_MPU6050_ReadAll(&IMU_datastruct);
}


// function that returns the acceleration in the Y direction
float get_accY(void)
{
	/* convert acceleration reading to [m/(s^2)] */
	accY = IMU_datastruct.Accelerometer_Y*9.81/32767.0;
	/* test code below */
	// accY = IMU_datastruct.Accelerometer_Y*;
	// accY = 9.81;
	
	/* return the Y acceleration */
	return accY;
}

/* get Z acceleration */
float get_accZ(void)
{	
	/* convert acceleration reading to [m/(s^2)] */
	accZ = IMU_datastruct.Accelerometer_Z*9.81/32767.0;
	
	/* return the Y acceleration */
	return accZ;
}


/* get X angular velocity */
float get_gyroX(void)
{
	gyroX = IMU_datastruct.Gyroscope_X*250.0*(M_PI/180)/32767.0;
	
	/* return the Z acceleration */
	return gyroX;
}

/* estimate the angle of the IMU using the Y and Z acceleration readings */
double get_acc_angle(void)
{
	/* retrieve values for accZ and accY */
	accZ = get_accZ();
	accY = get_accY();

	/* compute the IMU angle using accY and accZ using atan2 */
	acc_angle = -atan2(accZ, accY);
	
	/* return the IMU angle */
	return acc_angle;
}

