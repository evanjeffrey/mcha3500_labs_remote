// data_logging.c
// includes
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
// #include "pendulum.h"		// for reading the potentiometer voltage
#include "cmsis_os2.h"		// for the timer
#include "data_logging.h"	// also for the timer
#include "uart.h"
#include "drive_robot_motors.h"
#include "IMU.h"
#include "robot_encoders.h"
#include "measure_currents.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif
#define GEAR_RATIO 20.4086
#define COUNT_RES 48.0

static uint8_t _is_init = 0;
static uint8_t _is_init_1 = 0;
static uint8_t _is_init_2 = 0;
static uint8_t _is_init_4_sin = 0;

float torque1_in;
float torque2_in;
float voltage1_in;
float voltage2_in;
float frequency1_in;
float frequency2_in;
// float direction;
// float motor_num;

float phi1_0;
float phi1;
float dphi1;

float phi2_0;
float phi2;
float dphi2;

// static uint8_t _is_init_m2_enc = 0;

// variable declarations
uint16_t logCount;
static void (*log_function)(void);

// function declarations
// static void log_pendulum(void);
static void log_pointer(void *argument);
static void log_imu(void);
static void log_encoder1(void);
static void log_encoder2(void);
static void log_currents(void);
static void log_all(void);
static void log_all_motor1(void);
static void log_all_motor2(void);
static void log_torque1(void);
static void log_torque2(void);
static void log_sinTorque1(void);
static void log_sinTorque2(void);
static void log_sinVoltage1(void);
static void log_sinVoltage2(void);

// float dphi1_getValue(void);

// timer declarations
static osTimerId_t _loggingTimerID;
static osTimerAttr_t dataLogTimAttr = {
    .name = "dataLogTimer" 
};

// initialise the logging timer
void logging_init(void)
{
	/* initialise timer for use with pendulum data logging*/
	_loggingTimerID = osTimerNew(log_pointer, osTimerPeriodic, NULL, &dataLogTimAttr);
}

static void log_pointer(void *argument)
{
	UNUSED(argument);
	
	/* call function pointed to by log_function */
	(*log_function)();
}

// stop the timer
void logging_stop(void)
{
	/* make sure timer was already running. If not, can stop the timer */
	if (osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStop(_loggingTimerID);
	}
}










/* ================================================ */
/* IMU start */
void imu_logging_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_imu;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_imu(void)
{
	/* read IMU */
	IMU_read();
	
	/* get the IMU angle from accelerometer readings */
	double acc_angle = get_acc_angle();
	
	/* todo: get the IMU X gyro reading */
	float gyroX = get_gyroX();
	
	/* read the swing rig encoder values */
	int32_t swingEnc_count = motor2_encoder_getValue();
	
	/* convert encoder counts to an angle in radians */
	float swingEnc_angle = ((float)swingEnc_count/4096.0)*2.0*M_PI;
	
	/* print the time, accelerometer angle, gyro angular velocity and pot voltage values to the serial terminal in the format %f, %f, %f, %f\n */
	printf("%f, %f, %f, %f \n", ((float)logCount/200), acc_angle, gyroX, swingEnc_angle);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* encoder 1 start */
void encoder1_logging_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_encoder1;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_encoder1(void)
{
	// COME BACK LATER AND MAKE THIS INTO AN ARRAY, SAME STRUCTURE AS THE CURRENT SENSORS
	
	/* read the encoder value */
	int32_t encoder1Val = motor1_encoder_getValue();
	
	/* set the initial angle value */
	if(!_is_init_1) /* first time the function is run */
	{
		phi1_0 = 0.0;
	}
	else
	{
		phi1_0 = phi1; 
	}
	
	/* note that the sample frequency is 200 [Hz], so the sample time is 0.005 [s] */
	
	/* convert to an angle in radians*/
	phi1 = encoder1Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES);
	// phi1 = encoder1Val*(1/GEAR_RATIO);
	
	/* estimate the angular velocity in radians per second */
	dphi1 = (phi1 - phi1_0)/0.005;
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f\n", ((float)logCount/200), phi1, dphi1);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* encoder 2 start */
void encoder2_logging_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_encoder2;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_encoder2(void)
{
	// COME BACK LATER AND MAKE THIS INTO AN ARRAY, SAME STRUCTURE AS THE CURRENT SENSORS
	/* read the encoder value */
	int32_t encoder2Val = motor2_encoder_getValue();
	
	/* set the initial angle value */
	if(!_is_init_2) /* first time the function is run */
	{
		phi2_0 = 0.0;
	}
	else
	{
		phi2_0 = phi2;
	}
	
	/* note that the sample frequency is 200 [Hz], so the sample time is 0.005 [s] */
	
	/* convert to an angle in radians, and also convert to the wheel side of the gearbox*/
	phi2 = encoder2Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES);
	// phi1 = encoder1Val*(1/GEAR_RATIO);
	
	/* estimate the angular velocity in radians per second */
	dphi2 = (phi2 - phi2_0)/0.005;
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f\n", ((float)logCount/200), phi2, dphi2);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_2 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}









/* ================================================ */
/* both currents start */
void currents_logging_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_currents;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_currents(void)
{	
	/* declare the currents array */
	float currents[2];
	
	/* call the function to fill the values */
	read_currents(currents);
	
	float current1 = currents[0];	/* function from measure_currents.c */
	float current2 = currents[1];	/* function from measure_currents.c */
	// float torque = currents[1];	/* function from measure_currents.c */
	
	/* print the sample time and current from motor 1 to serial terminal, format [time], [current] */
	printf("%f, %f, %f\n", ((float)logCount/200), current1, current2);
	
	/* increment log count - this is the number of samples taken */
	logCount = logCount + 1;
	
	/* stop logging once 2 seconds (how to know this? use the system ticks?) is reached (complete this once the stop function is created) */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* velocities and currents from both motors at once start */
void all_logging_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_all;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_all(void)
{	
	// ENCODERS
	/* read encoder counts */
	int32_t encoder1Val = motor1_encoder_getValue(); 			/* encoder 1 */
	int32_t encoder2Val = motor2_encoder_getValue(); 			/* encoder 2 */
	
	/* set the initial angle values	*/
		if(!_is_init)
		{
			phi1_0 = 0.0;
			phi2_0 = 0.0;
		}
		else
		{
			phi1_0 = phi1;
			phi2_0 = phi2;
		}
	
	/* convert counts to radians */
	phi1 = encoder1Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES); 	/* encoder 1 */
	phi2 = encoder2Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES);		/* encoder 2 */

	/* estimate angular velocity based on angles */
	dphi1 = (phi1 - phi1_0)/0.005; 								/* encoder 1 */
	dphi2 = (phi2 - phi2_0)/0.005;								/* encoder 2 */
	
	
	// CURRENT SENSORS
	/* declare the currents array */
	float currents[2];
	
	/* call the function to fill the values */
	read_currents(currents);
	
	float current1 = currents[0];	/* function from measure_currents.c */
	float current2 = currents[1];	/* function from measure_currents.c */
	// float torque = currents[2];		/* function from measure_currents.c */
	
	
	// FINISHING UP
	/* print [sample time], [motor 1 velocity], [motor 1 current], [motor 2 velocity], [motor 2 current] */
	printf("%f, %f, %f, f, %f\n", ((float)logCount/200), dphi1, current1, dphi2, current2);
	
	/* increment log count - this is the number of samples taken */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init = 1;
	
	/* stop logging once 2 seconds (how to know this? use the system ticks?) is reached (complete this once the stop function is created) */
	if(logCount==1000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* velocities and currents from motor 1 */
void all_logging_motor1_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_all_motor1;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_all_motor1(void)
{	
	// ENCODERS
	/* read encoder counts */
	int32_t encoder1Val = motor1_encoder_getValue(); 			/* encoder 1 */
	
	/* set the initial angle values	*/
		if(!_is_init)
		{
			phi1_0 = 0.0;
		}
		else
		{
			phi1_0 = phi1;
		}
	
	/* convert counts to radians */
	phi1 = encoder1Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES); 	/* encoder 1 on wheel side of gearbox */

	/* estimate angular velocity based on angles */
	dphi1 = (phi1 - phi1_0)/0.005; 								/* encoder 1 */
	
	
	// CURRENT SENSORS
	/* declare the currents array */
	float currents[2];
	
	/* call the function to fill the values */
	read_currents(currents);
	
	float current1 = currents[0];	/* function from measure_currents.c */
	float current2 = currents[1];	/* function from measure_currents.c */
	
	
	// FINISHING UP
	/* print [sample time], [motor 1 velocity], [motor 1 current], [motor 2 velocity], [motor 2 current] */
	printf("%f, %f, %f\n", ((float)logCount/200), dphi1, current1);
	
	/* increment log count - this is the number of samples taken */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init = 1;
	
	/* stop logging once 2 seconds (how to know this? use the system ticks?) is reached (complete this once the stop function is created) */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* velocities and currents from motor 2 */
void all_logging_motor2_start(void)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_all_motor2;
	
	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5);
	}
}
static void log_all_motor2(void)
{	
	// ENCODERS
	/* read encoder counts */
	int32_t encoder2Val = motor2_encoder_getValue(); 			/* encoder 2 */
	
	/* set the initial angle values	*/
		if(!_is_init)
		{
			phi2_0 = 0.0;
		}
		else
		{
			phi2_0 = phi2;
		}
	
	/* convert counts to radians */
	phi2 = encoder2Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES); 	/* encoder 1 */

	/* estimate angular velocity based on angles */
	dphi2 = (phi2 - phi2_0)/0.005; 								/* encoder 1 */
	
	
	// CURRENT SENSORS
	/* declare the currents array */
	float currents[2];
	
	/* call the function to fill the values */
	read_currents(currents);
	
	float current1 = currents[0];	/* function from measure_currents.c */
	float current2 = currents[1];	/* function from measure_currents.c */
	
	
	// FINISHING UP
	/* print [sample time], [motor 1 velocity], [motor 1 current], [motor 2 velocity], [motor 2 current] */
	printf("%f, %f, %f\n", ((float)logCount/200), dphi2, current2);
	
	/* increment log count - this is the number of samples taken */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init = 1;
	
	/* stop logging once 2 seconds (how to know this? use the system ticks?) is reached (complete this once the stop function is created) */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










void sinVoltage1_logging_start(float V1, float f1)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_sinVoltage1;
	
	/* set torque_in input as a global variable */
	voltage1_in = V1;
	frequency1_in = f1;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_sinVoltage1(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder1Val = motor1_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_1) /* first time the function is run */
		{
			phi1_0 = 0.0;
		}
		else
		{
			phi1_0 = phi1;
		}
		
		/* convert to an angle in radians */
		phi1 = encoder1Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi1 = (phi1 - phi1_0)/0.005;
		
	// GET THE CURRENTS
		// CURRENT SENSORS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
	
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w1 = 0.305922;
		float R_a1 = 6.337309;
		float K_I1 = 0.0051;
		
		/* turn this voltage into a sine wave */
		float v = voltage1_in*sin(frequency1_in*2.0*M_PI*((float)logCount/200.0));
		
		/* send to set_voltage */
		set_motor1_voltage(v);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), v, dphi1, current1);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==1000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}









void sinVoltage2_logging_start(float V2, float f2)
{
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_sinVoltage2;
	
	/* set torque_in input as a global variable */
	voltage2_in = V2;
	frequency2_in = f2;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_sinVoltage2(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder2Val = motor2_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_1) /* first time the function is run */
		{
			phi2_0 = 0.0;
		}
		else
		{
			phi2_0 = phi2;
		}
		
		/* convert to an angle in radians */
		phi2 = encoder2Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi2 = (phi2 - phi2_0)/0.005;
		
	// GET THE CURRENTS
		// CURRENT SENSORS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w2 = 0.302306;
		float R_a2 = 6.177036;
		float K_I2 = 0.0048;
		
		/* turn this voltage into a sine wave */
		float v = voltage2_in*sin(frequency2_in*2.0*M_PI*((float)logCount/200.0));
		
		/* send to set_voltage */
		set_motor2_voltage(v);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), v, dphi2, current2);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}








/* ================================================ */
/* constant torque motor 1 input start */
void torque1_logging_start(float T1)
{	
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_torque1;
	
	/* set torque_in input as a global variable */
	torque1_in = T1;
	// direction = direction_in;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_torque1(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder1Val = motor1_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_1) /* first time the function is run */
		{
			phi1_0 = 0.0;
		}
		else
		{
			phi1_0 = phi1;
		}
		
		/* convert to an angle in radians */
		phi1 = encoder1Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi1 = (phi1 - phi1_0)/0.005;
		
	// GET THE CURRENTS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w1 = 0.305922;
		float R_a1 = 6.337309;
		float K_I1 = 0.0051;
		
		float amplitude1;
		
		if(dphi1 > 0)
		{
			amplitude1 = 0.032*K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		}
		else
		{
			amplitude1 = -0.032*K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		}
		
	/* since no sine wave, pump amplitude straight into voltage demand */
		set_motor1_voltage(amplitude1);
		
	// if(dphi1 > 0)
	// {
		// dphi1 = dphi1;
	// }
	// else
	// {
		// dphi1 = dphi1*(-1);
	// }
	
	/* if give negative torque, want a negative dphi */
		// if(torque1_in < 0) /* motor 1 should reverse, but dphi will be positive */
		// {
			// dphi1 = dphi1*(-1);
		// }
		// else
		// {
			// dphi1 = dphi1;
		// }
	
		/* compute required voltage */
		// float v = K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		
		/* send to set_voltage */
		// set_motor1_voltage(v);
	
	// GET THE CURRENTS
		// /* declare the currents array */
		// float currents[2];
		
		// /* call the function to fill the values */
		// read_currents(currents);
		
		// float current1 = currents[0];	/* function from measure_currents.c */
		// float current2 = currents[1];	/* function from measure_currents.c */
	
	// dphi1_getValue(dphi1);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), amplitude1, dphi1, current1);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}








/* ================================================ */
/* constant torque motor 1 input start */
void torque2_logging_start(float T2)
{	
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_torque2;
	
	/* set torque_in input as a global variable */
	torque2_in = T2;
	// direction = direction_in;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_torque2(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder2Val = motor2_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_1) /* first time the function is run */
		{
			phi2_0 = 0.0;
		}
		else
		{
			phi2_0 = phi2;
		}
		
		/* convert to an angle in radians */
		phi2 = encoder2Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi2 = (phi2 - phi2_0)/0.005;
		
	// GET THE CURRENTS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w2 = 0.302306;
		float R_a2 = 6.177036;
		float K_I2 = 0.0048;
		
		float amplitude2;
		
		if(dphi2 > 0)
		{
			amplitude2 = 0.032*K_w2*dphi2 + (R_a2*torque2_in)/(GEAR_RATIO*K_I2);
		}
		else
		{
			amplitude2 = -0.032*K_w2*dphi2 + (R_a2*torque2_in)/(GEAR_RATIO*K_I2);
		}
		
	/* since no sine wave, pump amplitude straight into voltage demand */
		set_motor2_voltage(amplitude2);
		
	// if(dphi1 > 0)
	// {
		// dphi1 = dphi1;
	// }
	// else
	// {
		// dphi1 = dphi1*(-1);
	// }
	
	/* if give negative torque, want a negative dphi */
		// if(torque1_in < 0) /* motor 1 should reverse, but dphi will be positive */
		// {
			// dphi1 = dphi1*(-1);
		// }
		// else
		// {
			// dphi1 = dphi1;
		// }
	
		/* compute required voltage */
		// float v = K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		
		/* send to set_voltage */
		// set_motor1_voltage(v);
	
	// GET THE CURRENTS
		// /* declare the currents array */
		// float currents[2];
		
		// /* call the function to fill the values */
		// read_currents(currents);
		
		// float current1 = currents[0];	/* function from measure_currents.c */
		// float current2 = currents[1];	/* function from measure_currents.c */
	
	// dphi1_getValue(dphi1);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), amplitude2, dphi2, current2);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}










/* ================================================ */
/* sinusoid torque motor 1 input start */
void sinTorque1_logging_start(float T1, float f1)
{	
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_sinTorque1;
	
	/* set torque_in input as a global variable */
	torque1_in = T1;
	frequency1_in = f1;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_sinTorque1(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder1Val = motor1_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_4_sin) /* first time the function is run */
		{
			phi1_0 = 0.0;
		}
		else
		{
			phi1_0 = phi1;
		}
		
		/* convert to an angle in radians */
		phi1 = encoder1Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi1 = (phi1 - phi1_0)/0.005;
		
	// GET THE CURRENTS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w1 = 0.015046;
		float R_a1 = 6.221535;
		float K_I1 = 0.0051;
		
		// if(dphi1 < 0) /* motor 1 should reverse, but dphi will be positive */
		// {
			// torque1_in = torque1_in*(-1);
		// }
		// else
		// {
			// torque1_in = torque1_in;
		// }
		
		/* compute required voltage amplitude */
		/* note that if T1 is neg, this gives a neg dphi1, which keeps the equation 'proportional' */
		// float amplitude1 = K_w1*(dphi1) + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		
		/* calculate required current from the torque */
		// float current1 = torque1_in/(N*K_I1);
		
		// torque1_in = 0.08;
		
		// float amplitude1 = 0.032*K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		
		/* test with just velocity input */
		float amplitude1;
		
		if(dphi1 > 0)
		{
			amplitude1 = K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		}
		else
		{
			amplitude1 = -K_w1*dphi1 + (R_a1*torque1_in)/(GEAR_RATIO*K_I1);
		}
		
		
		
		/* turn this voltage into a sine wave */
		float v1 = amplitude1*sin(frequency1_in*2.0*M_PI*((float)logCount/200.0));
		// float amplitude1 = 2.0 + 0.5*K_w1*dphi1;
		// float amplitude1 = 2.0;
		// float v1 = amplitude1;
		
		/* send to set_voltage */
		set_motor1_voltage(v1);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), phi1, dphi1, current1);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_4_sin = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}




/* ================================================ */
/* sinusoid torque motor 1 input start */
void sinTorque2_logging_start(float T2, float f2)
{	
	/* change function pointer to the IMU logging function (log_imu) */
	log_function = &log_sinTorque2;
	
	/* set torque_in input as a global variable */
	torque2_in = T2;
	frequency2_in = f2;
	// motor_num = motor_num_in;

	/* reset the log counter */
	logCount = 0;
	
	/* start data logging at 200 [Hz] (make sure timer was not already running first) */
	if (!osTimerIsRunning(_loggingTimerID))
	{
		/* start data logging timer at 200 [Hz] */
		osTimerStart(_loggingTimerID, 5); /* where 5 is the time ticks of the timer. 1 second is 1000 ticks, so 200 [Hz] is 1000/200 = 5 */
	}
}
static void log_sinTorque2(void)
{
	// DO ENCODER TO GET VELOCITY
		/* read the encoder value */
		int32_t encoder2Val = motor2_encoder_getValue();
		
		/* set the initial angle value */
		if(!_is_init_1) /* first time the function is run */
		{
			phi2_0 = 0.0;
		}
		else
		{
			phi2_0 = phi2;
		}
		
		/* convert to an angle in radians */
		phi2 = encoder2Val*2*M_PI*(1/COUNT_RES); /* gives on the high side of the gearbox */
		
		/* estimate the angular velocity in radians per second */
		dphi2 = (phi2 - phi2_0)/0.005;
		
	// GET THE CURRENTS
		/* declare the currents array */
		float currents[2];
		
		/* call the function to fill the values */
		read_currents(currents);
		
		float current1 = currents[0];	/* function from measure_currents.c */
		float current2 = currents[1];	/* function from measure_currents.c */
		
	// CALCULATE THE VOLTAGE USING THE TORQUE GLOBAL REFERENCE VALUE
		/* define parameters of the motor */
		float K_w2 = 0.015156;
		float R_a2 = 6.071850;
		float K_I2 = 0.0048;
		
		float amplitude2;
		
		if(dphi2 > 0)
		{
			amplitude2 = K_w2*dphi2 + (R_a2*torque2_in)/(GEAR_RATIO*K_I2);
		}
		else
		{
			amplitude2 = -K_w2*dphi2 + (R_a2*torque2_in)/(GEAR_RATIO*K_I2);
		}
		
		/* compute required voltage amplitude */
		/* note that if T1 is neg, this gives a neg dphi1, which keeps the equation 'proportional' */
		// float amplitude2 = K_w2*(dphi2) + (R_a2*torque2_in)/(GEAR_RATIO*K_I2);
		
		/* turn this voltage into a sine wave */
		float v2 = amplitude2*sin(frequency2_in*2.0*M_PI*((float)logCount/200.0));
		
		/* send to set_voltage */
		set_motor2_voltage(v2);
	
	/* print encoder angle and 'averaged' encoder angular velocity to screen */
	printf("%f, %f, %f, %f\n", ((float)logCount/200), v2, dphi2, current2);
	
	/* increment log count */
	logCount = logCount + 1;
	
	/* set the de-initial value so the iterative process will start */
	_is_init_1 = 1;
	
	/* stop logging once 5 seconds is reached */
	if(logCount==2000)
	{
		logging_stop();	/* call the function to stop the logging */
	}	
}






































/* start logging a constant torque */






// create a function that reads the potentiometer voltage (and also reads the sample time?) and prints it to the serial port
// static void log_pendulum(void)
// {
	/* supress compiler "unused variable" warnings */
	// UNUSED(argument);
	
	/* read the potentiometer voltage */
	// float voltage = pendulum_read_voltage();	/* function from pendulum.c */
	
	// /* print the sample time and potentiometer voltage to serial terminal, format [time], [voltage] */
	// printf("%f, %f \n", ((float)logCount/200), voltage);
	
	// /* increment log count - this is the number of samples taken */
	// logCount = logCount + 1;
	
	// /* stop logging once 2 seconds (how to know this? use the system ticks?) is reached (complete this once the stop function is created) */
	// if(logCount==400)
	// {
		// logging_stop();	/* call the function to stop the logging */
	// }	
// }

// next we will create an osTimer for producing data samples at a fixed frequency
// oncfigure the timer to be periodic

// start the timer (start the timer at 200 [Hz] frequency)
// void pend_logging_start(void)
// {
	// /* change function pointer to be the pendulum logging function */
	// log_function = &log_pendulum;
	
	// /* reset the log counter */
	// logCount = 0; 		/* what is the purpose of doing this? */
	
	// /* make sure timer was not already running. If not, can start the timer */
	// if (!osTimerIsRunning(_loggingTimerID))
	// {
		// /* start data logging timer at 200 [Hz] */
		// osTimerStart(_loggingTimerID, 5);
	// }
// }



/* for logging potentiometer data, angle measurements from accelerometer and X axis gyro measurements at 200 [Hz] */
/* note the potentiometer readings are used as a 'ground truth' position reference for calibrating the IMU */




// static void log_full_IMU(void)
// {
	/* COME BACK LATER AND MAKE THIS INTO AN ARRAY, SAME STRUCTURE AS THE CURRENT SENSORS */
	// /* read the encoder value */
	// int32_t encoder2Val = motor2_encoder_getValue();
	
	// /* set the initial angle value */
	// if(!_is_init_2) /* first time the function is run */
	// {
		// phi2_0 = 0.0;
	// }
	// else
	// {
		// phi2_0 = phi2;
	// }
	
	// /* note that the sample frequency is 200 [Hz], so the sample time is 0.005 [s] */
	
	// /* convert to an angle in radians*/
	// phi2 = encoder2Val*2*M_PI*(1/GEAR_RATIO)*(1/COUNT_RES);
	/* phi1 = encoder1Val*(1/GEAR_RATIO); */
	
	// /* estimate the angular velocity in radians per second */
	// dphi2 = (phi2 - phi2_0)/0.005;
	
	// /* print encoder angle and 'averaged' encoder angular velocity to screen */
	// printf("%f, %f, %f\n", ((float)logCount/200), phi2, dphi2);
	
	// /* increment log count */
	// logCount = logCount + 1;
	
	// /* set the de-initial value so the iterative process will start */
	// _is_init_2 = 1;
	
	// /* stop logging once 5 seconds is reached */
	// if(logCount==2000)
	// {
		// logging_stop();	/* call the function to stop the logging */
	// }	
// }
