// measure_currents.c
// includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "uart.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "measure_currents.h"

// definitions
static uint8_t _is_init = 0;

// declarations
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;

// ADC8: current1
// ADC9: current2


// Initialisation function for both motors
void current_sense_init(void)
{
	if (!_is_init)		/* since we only need to initialise the hardware once */
	{
	// MOTOR 1: PB0
	// MOTOR 2: PB1
	/* initialise both pins and channels at the same time? Is this okay? */
		/* enable ADC1 clock */
		__HAL_RCC_ADC1_CLK_ENABLE();
		
		/* enable GPIOB clock */
		__HAL_RCC_GPIOB_CLK_ENABLE();
		
		/* FOR TORQUE MEASUREMENTS */
		/* enable GPIOC clock */
		// __HAL_RCC_GPIOC_CLK_ENABLE();
		
		// initialise pins for the current measurements
		GPIO_InitTypeDef  GPIO_InitStructure_1;
		GPIO_InitStructure_1.Pin = GPIO_PIN_0|GPIO_PIN_1;		/* Pin 0 and 1 */
		GPIO_InitStructure_1.Mode = GPIO_MODE_ANALOG;			/* analog mode */
		GPIO_InitStructure_1.Pull = GPIO_NOPULL;				/* no pull */
		// GPIO_InitStructure_1.Speed = GPIO_SPEED_FREQ_HIGH;	/* high frequency */
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure_1);			/* initialise PB0 and PB1 */
		
		// initialise pin PC4 for the torque measurements (only for sysID)
		// GPIO_InitTypeDef  GPIO_InitStructure_2;
		// GPIO_InitStructure_2.Pin = GPIO_PIN_4;					/* Pin PC4 */
		// GPIO_InitStructure_2.Mode = GPIO_MODE_ANALOG;			/* analog mode */
		// GPIO_InitStructure_2.Pull = GPIO_NOPULL;				/* no pull */
		// GPIO_InitStructure_1.Speed = GPIO_SPEED_FREQ_HIGH;	/* high frequency */
		// HAL_GPIO_Init(GPIOC, &GPIO_InitStructure_2);			/* initialise PC4 */

	
		
		/*  */
		// HAL_ADC_Init(&hadc1);								/* initialise ADC0 */
		hadc1.Instance = ADC1;									/* instance ADC0 */
		hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;	/* div 2 prescaler */
		hadc1.Init.Resolution = ADC_RESOLUTION_12B;				/* 12 bit resolution */
		hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;				/* data align right */
		hadc1.Init.ScanConvMode = ENABLE;						/* scan conversion mode enabled */
		hadc1.Init.ContinuousConvMode = DISABLE;				/* continuous conversion mode disabled */
		hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;			/* continuous conversion mode disabled */
		hadc1.Init.NbrOfConversion = 2;							/* number of conversions = 3, including the torque sensor */

		sConfigADC.Channel = ADC_CHANNEL_8;						/* channel 8 for PB0 RTFM!!!!!!!!! */
		sConfigADC.Rank = 1;									/* rank 1 */
		sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;		/* sampling time 480 cycles */
		sConfigADC.Offset = 0;									/* offset 0 */
		
		HAL_ADC_Init(&hadc1); 									/* initialise the ADC */
		
		HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);				/* configure ADC channel */
		
		sConfigADC.Channel = ADC_CHANNEL_9;						/* channel 9 for PB1 RTFM!!!!!!!!! */
		sConfigADC.Rank = 2;										/* rank 2 */
		HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);				/* configure ADC channel */
		
		// sConfigADC.Channel = ADC_CHANNEL_14;						/* channel 14 for PC4 RTFM!!!!!!!!! */
		// sConfigADC.Rank = 3;										/* rank 3 */
		// HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);				/* configure ADC channel */
		
		_is_init = 1; 	/* no more iterations of this function after this statement */
	}
}

// create a function which reads the current of motor 1 (PB0) and returns this as a float
void read_currents(float *currents)
{	
	HAL_ADC_Start(&hadc1);										/* start the ADC */
	
	// Current 1 (rank 1)
	HAL_ADC_PollForConversion(&hadc1, 0xFF);					/* poll for conversion; use timeout of 0xFF */
	uint16_t fetched_voltage1 = HAL_ADC_GetValue(&hadc1);		/* get the ADC value and stored it fetched_voltage */
	float voltage1 = ((float)fetched_voltage1)*(3.3/4095.0);
	float current1 = (voltage1 - 2.4376)/0.4; 		 			/* 2.4376 is the constant voltage offset of current sensor 1 */
	
	// Torque (rank 1), whichever is not being used
	// HAL_ADC_PollForConversion(&hadc1, 0xFF);								/* poll for conversion for the torque sensor; use timeout of 0xFF */
	// uint16_t fetched_torque_voltage = HAL_ADC_GetValue(&hadc1);				/* get the ADC value and stored it fetched_voltage */
	// float torque_voltage = ((float)fetched_torque_voltage)*(3.3/4095.0); 	/* this is the voltage at the pin */
	// DO NOT USE, MATLAB DOES THIS float torque = (6/2.645)*(torque_voltage - 0.305) - 3.0;				/* proportional conversion from voltage output to torque */
	
	
	// Current 2 (rank 2)
	HAL_ADC_PollForConversion(&hadc1, 0xFF);					/* poll for conversion; use timeout of 0xFF */
	uint16_t fetched_voltage2 = HAL_ADC_GetValue(&hadc1);		/* get the ADC value and stored it fetched_voltage */
	float voltage2 = ((float)fetched_voltage2)*(3.3/4095.0);
	float current2 = (voltage2 - 2.4629)/0.4; 					/* 2.4629 is the constant voltage offset of current sensor 1 */
	
	HAL_ADC_Stop(&hadc1);										/* stop the ADC */
	
	currents[0] = (float)current1;
	
	currents[1] = (float)current2;
	
}





























