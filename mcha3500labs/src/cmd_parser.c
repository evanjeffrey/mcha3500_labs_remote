#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <inttypes.h> // For PRIxx and SCNxx macros
#include "stm32f4xx_hal.h" // to import UNUSED() macro
#include "cmd_line_buffer.h"
#include "cmd_parser.h"
#include "pendulum.h" // use the function which already reads the potentiometer voltage
#include "data_logging.h"
#include "IMU.h"
#include "drive_robot_motors.h"
#include "motor_update.h"
#include "measure_currents.h"
#include "robot_controller.h"

// Type for each command table entry
typedef struct
{
    void (*func)(int argc, char *argv[]);   // Command function pointer
    const char * cmd;                       // Command name
    const char * args;                      // Command arguments syntax
    const char * help;                      // Command description
} CMD_T;

// Forward declaration for built-in commands
static void _help(int, char *[]);
static void _reset(int, char *[]);

// static void _cmd_getPotentiometerVoltage(int, char *[]);	/* declaration for voltage reading serial command */
// static void _cmd_data_logPot(int, char *[]);
static void _cmd_data_logIMU(int, char *[]);
static void _cmd_drive_motor1(int argc, char *argv[]);
static void _cmd_drive_motor2(int argc, char *argv[]);
// static void _cmd_drive_motor1_forward(int argc, char *argv[]);
// static void _cmd_drive_motor2_forward(int argc, char *argv[]);
// static void _cmd_drive_motor1_reverse(int argc, char *argv[]);
// static void _cmd_drive_motor2_reverse(int argc, char *argv[]);
static void _cmd_brake_motor1(int argc, char *argv[]);
static void _cmd_brake_motor2(int argc, char *argv[]);
static void _cmd_data_sinusoidVoltage1(int argc, char *argv[]);
static void _cmd_data_sinusoidVoltage2(int argc, char *argv[]);
static void _cmd_data_logEncoder1(int, char *[]);
static void _cmd_data_logEncoder2(int, char *[]);
static void _cmd_data_logCurrents(int, char *[]);
static void _cmd_data_logAll(int, char *[]);
static void _cmd_data_logAll_motor1(int, char *[]);
static void _cmd_data_logAll_motor2(int, char *[]);
static void _cmd_data_demandTorque1(int, char *[]);
static void _cmd_data_demandTorque2(int, char *[]);
static void _cmd_data_sinusoidTorque1(int, char *[]);
static void _cmd_data_sinusoidTorque2(int, char *[]);
static void _cmd_updateControl(int, char *[]);

// Modules that provide commands
#include "heartbeat_cmd.h"

// Command table
static CMD_T cmd_table[] =
{
    {_help              			, "help"            , ""                          , "Displays this help message"             } ,
    {_reset             			, "reset"           , ""                          , "Restarts the system."                   } ,
    {heartbeat_cmd      			, "heartbeat"   	, "[start|stop]"              , "Get status or start/stop heartbeat task"} ,
	{_cmd_drive_motor1              , "drive1"  	    , "[v]"                       , "Drives motor 1 with the direction and magnitude provided voltage"} ,
	{_cmd_drive_motor2              , "drive2"  	    , "[v]"                       , "Drives motor 2 with the direction and magnitude provided voltage"} ,
	{_cmd_data_logIMU      			, "logIMU"   		, ""              			  , "Log and print accel angle and gyro speed at 200 Hz for 5 seconds"} ,
	// {_cmd_drive_motor1_forward      , "forward1"  	    , "[v]"                       , "Drives motor 1 forwards with the provided voltage"} ,
	// {_cmd_drive_motor2_forward      , "forward2"  	    , "[v]"                       , "Drives motor 2 forwards with the provided voltage"} ,
	// {_cmd_drive_motor1_reverse      , "reverse1"  	    , "[v]"                       , "Drives motor 1 in reverse with the provided voltage"} ,
	// {_cmd_drive_motor2_reverse      , "reverse2"  	    , "[v]"                       , "Drives motor 2 in reverse with the provided voltage"} ,
	{_cmd_brake_motor1              , "brake1"  	    , ""              	          , "Stops motor 1"} ,
	{_cmd_brake_motor2              , "brake2"  	    , ""              	          , "Stops motor 2"} ,
	{_cmd_data_sinusoidVoltage1     , "sinVoltage1"  	, "[V1] [f1]"                 , "Drives motor 1 in a sinusoidal manner with the provided amplitude and frequency"} ,
	{_cmd_data_sinusoidVoltage2     , "sinVoltage2"  	, "[V2] [f2]"                 , "Drives motor 2 in a sinusoidal manner with the provided amplitude and frequency"} ,
	{_cmd_data_logEncoder1          , "logEncoder1"  	, ""              		  	  , "Log and print the encoder value of motor 1"} ,
	{_cmd_data_logEncoder2          , "logEncoder2"  	, ""              		      , "Log and print the encoder value of motor 2"} ,
	{_cmd_data_logCurrents          , "logCurrents"  	, ""              		      , "Log and print the current running through motors 1 and 2 respectively"} ,
	{_cmd_data_logAll_motor1        , "logAllmotor1"  	, ""              		      , "Log and print velocity and current from motor 1"} ,
	{_cmd_data_logAll_motor2        , "logAllmotor2"  	, ""              		      , "Log and print velocity and current from motor 2"} ,
	{_cmd_data_logAll               , "logAll"  	    , ""              		      , "Log and print currents and encoder values from both motors"} ,
	{_cmd_data_demandTorque1        , "torque1"  	    , "[T1]"              		  , "Set a torque to motor1"} ,
	{_cmd_data_demandTorque2        , "torque2"  	    , "[T2]"              		  , "Set a torque to motor2"} ,
	{_cmd_data_sinusoidTorque1      , "sinTorque1"  	, "[T] [f2]"              	  , "Set a sinusoidal torque to motor1"} ,
	{_cmd_data_sinusoidTorque2      , "sinTorque2"  	, "[T] [f2]"              	  , "Set a sinusoidal torque to motor1"} ,
	{_cmd_updateControl      		, "getControl"      , "[x1] [x2] [x3] [x4]"       , "Set first 4 values of state vector, increment controller, print control value"} ,
};
enum {CMD_TABLE_SIZE = sizeof(cmd_table)/sizeof(CMD_T)};
enum {CMD_MAX_TOKENS = 5};      // Maximum number of tokens to process (command + arguments)

	// {_cmd_getPotentiometerVoltage    , "getPot"   	, ""              			  , "Show the current potentiometer voltage reading"} ,
	// {_cmd_data_logPot      			, "logPot"   	, ""              			  , "Log and print the potentiometer volatge at 200 Hz for 2 seconds"} ,
	// {_cmd_data_logIMU      			, "logIMU"   	, ""              			  , "Log and print potentiometer voltage, accel angle and gyro speed at 200 Hz for 2 seconds"} ,
	// {_cmd_updateControl      		, "getControl"  , "[x1 x2 x3 x4]"             , "Set first 4 values of state vector, increment controller, print control value"} ,

// Command function definitions
/* create a serial command getControl used to set the first 4 values of the state vector, increment the controller then print the control value */



static void _print_chip_pinout(void);

void _help(int argc, char *argv[])
{
    UNUSED(argv);
    printf(
        "\n"
        "\n"
    );

    _print_chip_pinout();
    
    printf("\n");

    // Describe argument syntax using POSIX.1-2008 convention
    // see http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html
    switch (argc)
    {
    case 1:
        printf(
            "   Command Arguments            Description\n"
            "-------------------------------------------\n"
        );
        for (int i = 0; i < CMD_TABLE_SIZE; i++)
        {
            printf("%10s %-20s %s\n", cmd_table[i].cmd, cmd_table[i].args, cmd_table[i].help);
        }
        // printf("\nFor more information, enter help followed by the command name\n\n");
        break;
    case 2:
        printf("Not yet implemented.\n\n");
        // TODO: Scan command table, and lookup extended help string.
        break;
    default:
        printf("help is expecting zero or one argument.\n\n");
    }
}

void _reset(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);
    // Reset the system
    HAL_NVIC_SystemReset();
}

void _print_chip_pinout(void)
{
    printf(
        "Pin configuration:\n"
        "\n"
        "       .---------------------------------------.\n"
        " PC10--|  1  2 --PC11              PC9--  1  2 |--PC8\n"
        " PC12--|  3  4 --PD2               PB8--  3  4 |--PC6\n"
        "  VDD--|  5  6 --E5V               PB9--  5  6 |--PC5\n"
        "BOOT0--|  7  8 --GND              AVDD--  7  8 |--U5V\n"
        "   NC--|  9 10 --NC                GND--  9 10 |--NC\n"
        "   NC--| 11 12 --IOREF             PA5-- 11 12 |--PA12\n"
        " PA13--| 13 14 --RESET             PA6-- 13 14 |--PA11\n"
        " PA14--| 15 16 --+3v3              PA7-- 15 16 |--PB12\n"
        " PA15--| 17 18 --+5v               PB6-- 17 18 |--NC\n"
        "  GND--| 19 20 --GND               PC7-- 19 20 |--GND\n"
        "  PB7--| 21 22 --GND               PA9-- 21 22 |--PB2\n"
        " PC13--| 23 24 --VIN               PA8-- 23 24 |--PB1\n"
        " PC14--| 25 26 --NC               PB10-- 25 26 |--PB15\n"
        " PC15--| 27 28 --PA0               PB4-- 27 28 |--PB14\n"
        "  PH0--| 29 30 --PA1               PB5-- 29 30 |--PB13\n"
        "  PH1--| 31 32 --PA4               PB3-- 31 32 |--AGND\n"
        " VBAT--| 33 34 --PB0              PA10-- 33 34 |--PC4\n"
        "  PC2--| 35 36 --PC1               PA2-- 35 36 |--NC\n"
        "  PC3--| 37 38 --PC0               PA3-- 37 38 |--NC\n"
        "       |________                   ____________|\n"
        "                \\_________________/\n"
    );
}





static void _cmd_updateControl(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 5)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		/* note that the zero index is the input argument "getControl" */
		/* update states using ctrl_set_xi functions */
		ctrl_set_x1(atof(argv[1])); 		/* atof function converts from the input string to a floating point number */
		ctrl_set_x2(atof(argv[2]));
		ctrl_set_x3(atof(argv[3]));
		ctrl_set_x4(atof(argv[4]));
		
		/* todo: update controller value */
		ctrl_update();
		
		/* print control action */
		// printf("%f\n", getControl());
		printf("%f\n", 2.35	);
	}
}






/* Drive motor 1 with a constant torque */
void _cmd_data_demandTorque1(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 2)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		// uint16_t direction = 4;
		// uint16_t motor_num = 1;
		
		/* send to data logging first */
		torque1_logging_start(atof(argv[1]));
	}
}





/* Drive motor 1 with a constant torque */
void _cmd_data_demandTorque2(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 2)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		// uint16_t direction = 4;
		// uint16_t motor_num = 1;
		
		/* send to data logging first */
		torque2_logging_start(atof(argv[1]));
	}
}





/* Drive motor 1 with a sinusoidal torque */
void _cmd_data_sinusoidTorque1(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 3)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		// uint16_t direction = 4;
		// uint16_t motor_num = 1;
		
		/* send to data logging first */
		sinTorque1_logging_start(atof(argv[1]), atof(argv[2]));
	}
}


/* Drive motor 2 with a sinusoidal torque */
void _cmd_data_sinusoidTorque2(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 3)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		// uint16_t direction = 4;
		// uint16_t motor_num = 1;
		
		/* send to data logging first */
		sinTorque2_logging_start(atof(argv[1]), atof(argv[2]));
	}
}


/* Drive motor 1 with a sinusoidal voltage */
void _cmd_data_sinusoidVoltage1(int argc, char *argv[])
{
	if(argc != 3)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		/* supress compiler warnings for unused arguments */
		UNUSED(argc);
		UNUSED(argv);
		
		/* set the voltage (i.e. amplitude) of the sinusoid */
		// read_sinusoid_config(atof(argv[1]));
	
		/* start the motor driver timer function */
		sinVoltage1_logging_start(atof(argv[1]), atof(argv[2]));
	}
}





/* Drive motor 2 with a sinusoidal voltage */
void _cmd_data_sinusoidVoltage2(int argc, char *argv[])
{
	if(argc != 3 )
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		/* supress compiler warnings for unused arguments */
		UNUSED(argc);
		UNUSED(argv);
		
		/* set the voltage (i.e. amplitude) of the sinusoid */
		// read_sinusoid_config(atof(argv[1]));
	
		/* start the motor driver timer function */
		sinVoltage2_logging_start(atof(argv[1]), atof(argv[2]));
	}
}





/* drive motor 1 with a constant set voltage */
static void _cmd_drive_motor1(int argc, char *argv[])
{	
	/* check for correct input arguments */
	if(argc != 2)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		/* first argument is motor number, second argument is voltage */
		set_motor1_voltage(atof(argv[1])); 		/* atof function converts from the input string to a floating point number */
	}
}





/* drive motor 2 with a constant set voltage */
static void _cmd_drive_motor2(int argc, char *argv[])
{	
	/* check for correct input arguments */
	if(argc != 2)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		/* first argument is motor number, second argument is voltage */
		set_motor2_voltage(atof(argv[1])); 		/* atof function converts from the input string to a floating point number */
	}
}





/* brake motor 1 */
static void _cmd_brake_motor1(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 1)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		UNUSED(argc);
		UNUSED(argv);
		
		/* set voltage equal to 0.0 */
		set_motor1_voltage(0.0);
	}
}





/* brake motor 2 */
static void _cmd_brake_motor2(int argc, char *argv[])
{
	/* check for correct input arguments */
	if(argc != 1)
	{
		printf("Incorrect arguments\n");
	}
	else
	{
		UNUSED(argc);
		UNUSED(argv);
		
		/* set voltage equal to 0.0 */
		set_motor2_voltage(0.0);
	}
}





/* start logging the IMU */
void _cmd_data_logIMU(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	imu_logging_start();
}





/* log motor 1 angle and velocity */
void _cmd_data_logEncoder1(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	encoder1_logging_start();
}





/* log motor 2 angle and velocity */
void _cmd_data_logEncoder2(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	encoder2_logging_start();
}





/* log currents of both currents */
void _cmd_data_logCurrents(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	currents_logging_start();
}





/* log velocities and currents of both motors */
void _cmd_data_logAll(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	all_logging_start();
}





/* log velocity and current of motor 1 */
void _cmd_data_logAll_motor1(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	all_logging_motor1_start();
}





/* log velocity and current of motor 2 */
void _cmd_data_logAll_motor2(int argc, char *argv[])
{
	/* supress compiler warnings for unused arguments */
	UNUSED(argc);
	UNUSED(argv);
	
	all_logging_motor2_start();
}






// void _cmd_getPotentiometerVoltage(int argc, char *argv[])
// {
	// /* supress compiler warnings for unused arguments */
	// UNUSED(argc);
	// UNUSED(argv);
	
	// /* read the potentiometer voltage */
	// float voltage = pendulum_read_voltage();		/* call the function from pendulum.c */
	
	// /* print the voltage to the serial terminal */
	// printf("Potentometer voltage is %f [V]\n", voltage);
	
// }

// function for the logging of the pot voltage incrementally (data logging) for 2 seconds
// void _cmd_data_logPot(int argc, char *argv[])
// {
	// /* supress compiler warnings for unused arguments */
	// UNUSED(argc);
	// UNUSED(argv);
	
	// pend_logging_start();
// }

// static void _cmd_drive_motor1_forward(int argc, char *argv[])
// {
	// /* check for correct input arguments */
	// if(argc != 2)
	// {
		// printf("Incorrect arguments\n");
	// }
	// else
	// {
		// /* set the reverse flag for the set_voltage() function */
		// uint16_t direction = 4;
		// uint16_t motor_num = 1;
		
		// /* first argument is motor number, second argument is voltage */
		// set_voltage(atof(argv[1]), direction, motor_num); 		/* atof function converts from the input string to a floating point number */
	// }
// }



// static void _cmd_drive_motor2_forward(int argc, char *argv[])
// {	
	// /* check for correct input arguments */
	// if(argc != 2)
	// {
		// printf("Incorrect arguments\n");
	// }
	// else
	// {
		// /* set the reverse flag for the set_voltage() function */
		// uint16_t direction = 5;
		// uint16_t motor_num = 2;
		
		// /* first argument is motor number, second argument is voltage */
		// set_voltage(atof(argv[1]), direction, motor_num); 		/* atof function converts from the input string to a floating point number */
	// }
// }

// static void _cmd_drive_motor1_reverse(int argc, char *argv[])
// {
	// /* check for correct input arguments */
	// if(argc != 2)
	// {
		// printf("Incorrect arguments\n");
	// }
	// else
	// {
		// /* set the reverse flag for the set_voltage() function */
		// uint16_t direction = 5;
		// uint16_t motor_num = 1;
		
		// set_voltage(atof(argv[1]), direction, motor_num); 		/* atof function converts from the input string to a floating point number */
	// }
// }

// static void _cmd_drive_motor2_reverse(int argc, char *argv[])
// {	
	// /* check for correct input arguments */
	// if(argc != 2)
	// {
		// printf("Incorrect arguments\n");
	// }
	// else
	// {
		// /* set the reverse flag for the set_voltage() function */
		// uint16_t direction = 4;
		// uint16_t motor_num = 2;
		
		// /* note that the zero index is the input argument "getControl" */
		// /* update states using ctrl_set_xi functions */
		// set_voltage(atof(argv[1]), direction, motor_num); 		/* atof function converts from the input string to a floating point number */
	// }
// }


// static void _cmd_updateControl(int argc, char *argv[])
// {
	/* check for correct input arguments */
	// if(argc != 5)
	// {
		// printf("Incorrect arguments\n");
	// }
	// else
	// {
		// /* note that the zero index is the input argument "getControl" */
		// /* todo: update states using ctrl_set_xi functions */
		// ctrl_set_x1(atof(argv[1])); 		/* atof function converts from the input string to a floating point number */
		// ctrl_set_x2(atof(argv[2]));
		// ctrl_set_x3(atof(argv[3]));
		// ctrl_set_x4(atof(argv[4]));
		
		// /* todo: update controller value */
		// ctrl_update();
		
		// /* print control action */
		// printf("%f\n", getControl());
	// }
// }

// Command parser and dispatcher

static int _makeargv(char *s, char *argv[], int argvsize);

#ifdef NO_LD_WRAP
void cmd_parse(char *) __asm__("___real_cmd_parse");
#endif

void cmd_parse(char * cmd)
{
    if (cmd == NULL)
    {
        printf("ERROR: Tried to parse NULL command pointer\n");
        return;
    }
    else if (*cmd == '\0') // Empty command string
    {
        return;
    }

    // Tokenise command string
    char *argv[CMD_MAX_TOKENS];
    int argc = _makeargv(cmd, argv, CMD_MAX_TOKENS);

    // Execute corresponding command function
    for (int i = 0; i < CMD_TABLE_SIZE; i++)
    {
        if (strcmp(argv[0], cmd_table[i].cmd) == 0)
        {
            cmd_table[i].func(argc, argv);
            return;
        }
    }

    // Command not found
    printf("Unknown command: \"%s\"\n", argv[0]);
}

// Command tokeniser

int _makeargv(char *s, char *argv[], int argvsize)
{
    char *p = s;
    int argc = 0;

    for(int i = 0; i < argvsize; ++i)
    {
        // skip leading whitespace
        while (isspace(*p))
            p++;

        if(*p != '\0')
            argv[argc++] = p;
        else
        {
            argv[argc] = NULL;
            break;
        }

        // scan over arg
        while(*p != '\0' && !isspace(*p))
            p++;

        // terminate arg
        if(*p != '\0' && i < argvsize - 1)
            *p++ = '\0';
    }

    return argc;
}


