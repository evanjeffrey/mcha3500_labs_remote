// drive_robot_motors.c
// includes
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "drive_robot_motors.h"
#include "data_logging.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#define GEAR_RATIO 20.4086

// defines
// #define PERIOD 10000
float PWM1_1_duty_cycle;
float PWM1_2_duty_cycle;
float PWM2_1_duty_cycle;
float PWM2_2_duty_cycle;

// declare variables
// float duty_cycle;

/********************************************************************************************/
// MOTOR DRIVE
/* declare function handles */
// MOTOR 1
	static TIM_HandleTypeDef htim3; 				/* for both motor 1 PWM1 and PWM 2 */
	static TIM_OC_InitTypeDef sConfigPWM1_1;		/* for motor 1 PWM1 pin PA6 */
	static TIM_OC_InitTypeDef sConfigPWM1_2;		/* for motor 1 PWM2 pin PA7 */

// MOTOR 2
	static TIM_HandleTypeDef htim4;					/* for both motor 2 PWM1 and PWM 2 */
	static TIM_OC_InitTypeDef sConfigPWM2_1;		/* for motor 2 PWM1 pin PB8 */
	static TIM_OC_InitTypeDef sConfigPWM2_2;		/* for motor 2 PWM2 pin PB9 */


static uint8_t _is_init1 = 0;		/* set the initialisation variable so that motor1_PWM_init() will only run once */
static uint8_t _is_init2 = 0;		/* set the initialisation variable so that motor2_PWM_init() will only run once */
// int32_t enc_count1;				/* declare the motor 1 encoder count variable and start it at 0 */
// int32_t enc_count2;				/* declare the motor 2 encoder count variable and start it at 0 */

// MOTOR 1 and MOTOR 2 set duty cycle
// get the voltage from the serial terminal

// MOTOR 1 initialisation
/* initialise the pin */
void motor1_PWM_init(void)
{
	if(!_is_init1)
	{	
		// enable clock for GPIOA (timer and channel?)
		__TIM3_CLK_ENABLE();
		__GPIOA_CLK_ENABLE();
		
		
		// initialise pin PA6 (PWM1)
		GPIO_InitTypeDef  GPIO_InitStructure_1;
		GPIO_InitStructure_1.Pin = GPIO_PIN_6;									/* pin 6 */
		GPIO_InitStructure_1.Mode = GPIO_MODE_AF_PP;							/* alternate function push-pull mode */
		GPIO_InitStructure_1.Pull = GPIO_NOPULL;								/* no pull */
		GPIO_InitStructure_1.Speed = GPIO_SPEED_FREQ_HIGH;						/* high frequency */
		GPIO_InitStructure_1.Alternate = GPIO_AF2_TIM3;							/* alternate function 2 - timer3 */
		HAL_GPIO_Init(GPIOA, &GPIO_InitStructure_1);							/* initialise PA6 */

		htim3.Instance = TIM3;													/* instance TIM3 */
		htim3.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1;							/* prescalar of 1 */
		htim3.Init.CounterMode = TIM_COUNTERMODE_UP;							/* counter mode up */
		htim3.Init.Period = 10000;												/* timer period to generate a 10kHz signal */
		htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;						/* clock division of 0 */
		HAL_TIM_PWM_Init(&htim3);												/* initialise timer 3 */	
		
		sConfigPWM1_1.OCMode = TIM_OCMODE_PWM1;									/* output compare mode PWM1 */
		sConfigPWM1_1.Pulse = 0;												/* pulse = 0 */
		sConfigPWM1_1.OCPolarity = TIM_OCPOLARITY_HIGH;							/* OC polarity high */	
		sConfigPWM1_1.OCFastMode = TIM_OCFAST_DISABLE;							/* fast mode disabled */
		HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigPWM1_1, TIM_CHANNEL_1);		/* configure timer 3, channel 1 */
		
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);								/* start Timer 3, Channel 1 */
		
		
		// initialise pin PA7 (PWM2) (just set as an output pin high or low)
		GPIO_InitTypeDef  GPIO_InitStructure_2;
		GPIO_InitStructure_2.Pin = GPIO_PIN_7;									/* pin 7 */
		GPIO_InitStructure_2.Mode = GPIO_MODE_AF_PP;							/* alternate function push-pull mode */
		GPIO_InitStructure_2.Pull = GPIO_NOPULL;								/* no pull */
		GPIO_InitStructure_2.Speed = GPIO_SPEED_FREQ_HIGH;						/* high frequency */
		GPIO_InitStructure_2.Alternate = GPIO_AF2_TIM3;							/* alternate function 2 - timer 3 */
		HAL_GPIO_Init(GPIOA, &GPIO_InitStructure_2);							/* initialise PA7 */
		
		/* the following block (timer 3) was already initialised */
		// htim3.Instance = TIM3;													/* instance TIM3 */
		// htim3.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1;							/* prescalar of 1 */
		// htim3.Init.CounterMode = TIM_COUNTERMODE_UP;							/* counter mode up */
		// htim3.Init.Period = 10000;												/* timer period to generate a 10kHz signal */
		// htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;						/* clock division of 0 */
		// HAL_TIM_PWM_Init(&htim3);												/* initialise timer 3 */	
		
		sConfigPWM1_2.OCMode = TIM_OCMODE_PWM1;									/* output compare mode PWM1 CHECK THIS ONE */
		sConfigPWM1_2.Pulse = 0;												/* pulse = 0 */
		sConfigPWM1_2.OCPolarity = TIM_OCPOLARITY_HIGH;							/* OC polarity high */	
		sConfigPWM1_2.OCFastMode = TIM_OCFAST_DISABLE;							/* fast mode disabled */
		HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigPWM1_2, TIM_CHANNEL_2);		/* configure timer 3, channel 2 */
		
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);								/* start Timer 3, Channel 2 */
		
		
		// initialise pin PC5 (ENB)
		__HAL_RCC_GPIOC_CLK_ENABLE();
		GPIO_InitTypeDef GPIO_InitStructure_5;
		GPIO_InitStructure_5.Pin = GPIO_PIN_5;
		GPIO_InitStructure_5.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStructure_5.Pull = GPIO_PULLDOWN;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStructure_5);
		
		
		// initialise pin PC6 (EN)
		GPIO_InitTypeDef GPIO_InitStructure_6;
		GPIO_InitStructure_6.Pin = GPIO_PIN_6;
		GPIO_InitStructure_6.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStructure_6.Pull = GPIO_PULLUP;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStructure_6);
		
		_is_init1 = 1;				/* set this so the MOTOR 1 function will not run again */
	}
}
	
// MOTOR 2 initialisation
void motor2_PWM_init(void)
{
	/* PB8, PWM2_1, timer 4, channel 3 */
	/* PB9, PWM2_2, timer 4, channel 4 */
		
	if(!_is_init2)
	{	
		// enable clock for GPIOA (timer and channel?)
		__TIM4_CLK_ENABLE();
		__GPIOB_CLK_ENABLE();


		// initialise pin PB8 (PWM1)
		GPIO_InitTypeDef  GPIO_InitStructure_3;
		GPIO_InitStructure_3.Pin = GPIO_PIN_8;									/* pin 8 */
		GPIO_InitStructure_3.Mode = GPIO_MODE_AF_PP;							/* alternate function push-pull mode */
		GPIO_InitStructure_3.Pull = GPIO_NOPULL;								/* no pull */
		GPIO_InitStructure_3.Speed = GPIO_SPEED_FREQ_HIGH;						/* high frequency */
		GPIO_InitStructure_3.Alternate = GPIO_AF2_TIM4;							/* alternate function 2 - timer 4 */
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure_3);							/* initialise PB8 */

		htim4.Instance = TIM4;													/* instance TIM4 */
		htim4.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1;							/* prescalar of 1 */
		htim4.Init.CounterMode = TIM_COUNTERMODE_UP;							/* counter mode up */
		htim4.Init.Period = 10000;												/* timer period to generate a 10kHz signal */
		htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;						/* clock division of 0 */
		HAL_TIM_PWM_Init(&htim4);												/* initialise timer 4 */	
		
		sConfigPWM2_1.OCMode = TIM_OCMODE_PWM1;									/* output compare mode PWM1 */
		sConfigPWM2_1.Pulse = 0;												/* pulse = 0 */
		sConfigPWM2_1.OCPolarity = TIM_OCPOLARITY_HIGH;							/* OC polarity high */	
		sConfigPWM2_1.OCFastMode = TIM_OCFAST_DISABLE;							/* fast mode disabled */
		HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigPWM2_1, TIM_CHANNEL_3);		/* configure timer 4, channel 3 */
		
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);								/* start Timer 4, Channel 3 */
		
		
		// initialise pin PB9 (PWM2)
		GPIO_InitTypeDef  GPIO_InitStructure_4;
		GPIO_InitStructure_4.Pin = GPIO_PIN_9;									/* pin 9 */
		GPIO_InitStructure_4.Mode = GPIO_MODE_AF_PP;								/* alternate function push-pull mode */
		GPIO_InitStructure_4.Pull = GPIO_NOPULL;									/* no pull */
		GPIO_InitStructure_4.Speed = GPIO_SPEED_FREQ_HIGH;						/* high frequency */
		GPIO_InitStructure_4.Alternate = GPIO_AF2_TIM4;							/* alternate function 2 - timer 4 */
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure_4);								/* initialise PB9 */
		
		
		/* the following block (timer 4) was already initialised */
		// htim4.Instance = TIM4;													/* instance TIM1_2 */
		// htim4.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1;							/* prescalar of 1 */
		// htim4.Init.CounterMode = TIM_COUNTERMODE_UP;							/* counter mode up */
		// htim4.Init.Period = PERIOD;												/* timer period to generate a 10kHz signal */
		// htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;						/* clock division of 0 */
		// HAL_TIM_PWM_Init(&htim4);												/* initialise timer 4 */	
		
		sConfigPWM2_2.OCMode = TIM_OCMODE_PWM1;									/* output compare mode PWM1 CHECK THIS ONE */
		sConfigPWM2_2.Pulse = 0;												/* pulse = 0 */
		sConfigPWM2_2.OCPolarity = TIM_OCPOLARITY_HIGH;							/* OC polarity high */	
		sConfigPWM2_2.OCFastMode = TIM_OCFAST_DISABLE;							/* fast mode disabled */
		HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigPWM2_2, TIM_CHANNEL_4);		/* configure timer 4, channel 4 */
		
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);								/* start Timer 4, Channel 4 */
		
		
		// initialise pin PB4 (ENB)
		__HAL_RCC_GPIOB_CLK_ENABLE();
		GPIO_InitTypeDef GPIO_InitStructure_7;
		GPIO_InitStructure_7.Pin = GPIO_PIN_4;
		GPIO_InitStructure_7.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStructure_7.Pull = GPIO_PULLDOWN;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure_7);
		
		
		// initialise pin PB5 (EN)
		GPIO_InitTypeDef GPIO_InitStructure_8;
		GPIO_InitStructure_8.Pin = GPIO_PIN_5;
		GPIO_InitStructure_8.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStructure_8.Pull = GPIO_PULLUP;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure_8);
		
		_is_init2 = 1;				/* set this so the MOTOR 2 function will not run again */
	}
}

void set_motor1_voltage(float v) 	/* this voltage value will be used for both motors at this stage */
{
	/* protect against over-voltage */
	if(v > 12.0)
	{
		v = 12.0;
	}
	else if(v < -12.0)
	{
		v = -12.0;
	}
	else
	{
		v = v;
	}

	/* determine sign of voltage */
	if(v > 0) 			/* forwards */
	{
		PWM1_1_duty_cycle = (v/12.0)*10000.0;
		PWM1_2_duty_cycle = 0.0;
	}
	else if(v < 0)		/* reverse */
	{
		/* convert v to a positive number; duty cycle must be positive */
		v = v*(-1);
		PWM1_1_duty_cycle = 0.0;
		PWM1_2_duty_cycle = (v/12.0)*10000.0;
	}
	else 				/* brake */
	{
		PWM1_1_duty_cycle = v*0.0;
		PWM1_2_duty_cycle = v*0.0;
	}
	
	/* set the PWM2 pin duty cycle */
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (uint16_t)PWM1_1_duty_cycle);

	/* set the PWM2 pin duty cycle */
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (uint16_t)PWM1_2_duty_cycle);			/* set initial Timer 3, channel 1 compare value */
	
	/* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
	
	/* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
}

void set_motor2_voltage(float v) 	/* this voltage value will be used for both motors at this stage */
{
	/* protect against over-voltage */
	if(v > 12.0)
	{
		v = 12.0;
	}
	else if(v < -12.0)
	{
		v = -12.0;
	}
	else
	{
		v = v;
	}
	
	/* determine sign of voltage */
	if(v > 0) 			/* forwards, needs to be the opposite of motor 1 */
	{
		PWM2_2_duty_cycle = (v/12.0)*10000.0;
		PWM2_1_duty_cycle = 0.0;
	}
	else if(v < 0)		/* reverse */
	{
		/* convert v to a positive number; duty cycle must be positive */
		v = v*(-1);
		PWM2_2_duty_cycle = 0.0;
		PWM2_1_duty_cycle = (v/12.0)*10000.0;
	}
	else 				/* brake */
	{
		PWM2_2_duty_cycle = v*0.0;
		PWM2_1_duty_cycle = v*0.0;
	}
	
	/* set the duty cycle of PWM2_1 using the timer (note that (uint16_t) converts the float to uint16_t) */
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_3, (uint16_t)PWM2_1_duty_cycle);		/* set initial Timer 4, channel 3 compare value */
	
	/* set the duty cycle of PWM2_2 using the timer */
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_4, (uint16_t)PWM2_2_duty_cycle);				/* set initial Timer 4, channel 4 compare value */
	
	/* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
	
	/* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
}







	
	// MOTOR 1
	// if(motor_num == 1) 				/* set the params for motor 1 */
	// {

	// }
	
	// MOTOR 2
	// if(motor_num == 2)
	// {
		// set the direction logic for the driver
		// if(v > 0) 				/* forwards */
		// {
			// PWM2_1_duty_cycle = (v/12.0)*10000.0;
			// PWM2_2_duty_cycle = 0.0;
		// }
		// else if(v < 0)			/* reverse */
		// {
			// PWM2_1_duty_cycle = 0.0;
			// PWM2_2_duty_cycle = (v/12.0)*10000.0;
		// }
		// else
		// {
			// PWM2_1_duty_cycle = v*0.0;
			// PWM2_2_duty_cycle = v*0.0;
		// }
		
		/* set motor 1 drive to 0 */
		// PWM1_1_duty_cycle = 0.0;
		// PWM1_2_duty_cycle = 0.0;
		
		// DRIVE MOTOR 2

	// }
// }

// void set_voltage(float v, uint16_t direction, uint16_t motor_num) 	/* this voltage value will be used for both motors at this stage */
// {
	// determine which motor to drive
	
	// MOTOR 1
	// if(motor_num == 1) 				/* set the params for motor 1 */
	// {
		// set the direction logic for the driver
		// if(direction == 4) 			/* forwards */
		// {
			// PWM1_1_duty_cycle = (v/12.0)*10000.0;
			// PWM1_2_duty_cycle = 0.0;
		// }
		// else if(direction == 5)		/* reverse */
		// {
			// PWM1_1_duty_cycle = 0.0;
			// PWM1_2_duty_cycle = (v/12.0)*10000.0;
		// }
		// else
		// {
			// PWM1_1_duty_cycle = v*0.0;
			// PWM1_2_duty_cycle = v*0.0;
		// }
		
		// /* set motor 2 drive to 0 */
		// PWM2_1_duty_cycle = 0.0;
		// PWM2_2_duty_cycle = 0.0;
		
		// DRIVE MOTOR 1
		// /* set the duty cycle of PWM2_1 using the timer */
		// __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (uint16_t)PWM1_1_duty_cycle);
	
		// /* set the PWM2 pin to LOW (GPIO_PIN_RESET is LOW) */
		// __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (uint16_t)PWM1_2_duty_cycle);			/* set initial Timer 3, channel 1 compare value */
		
		// /* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
		// HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
		
		// /* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
		// HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	// }
	
	// MOTOR 2
	// if(motor_num == 2)
	// {
		// set the direction logic for the driver
		// if(direction == 4) 				/* forwards */
		// {
			// PWM2_1_duty_cycle = (v/12.0)*10000.0;
			// PWM2_2_duty_cycle = 0.0;
		// }
		// else if(direction == 5)			/* reverse */
		// {
			// PWM2_1_duty_cycle = 0.0;
			// PWM2_2_duty_cycle = (v/12.0)*10000.0;
		// }
		// else
		// {
			// PWM2_1_duty_cycle = v*0.0;
			// PWM2_2_duty_cycle = v*0.0;
		// }
		
		// /* set motor 1 drive to 0 */
		// PWM1_1_duty_cycle = 0.0;
		// PWM1_2_duty_cycle = 0.0;
		
		// DRIVE MOTOR 2
		// /* set the duty cycle of PWM2_1 using the timer (note that (uint16_t) converts the float to uint16_t) */
		// __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_3, (uint16_t)PWM2_1_duty_cycle);		/* set initial Timer 4, channel 3 compare value */
		
		// /* set the duty cycle of PWM2_2 using the timer */
		// __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_4, (uint16_t)PWM2_2_duty_cycle);				/* set initial Timer 4, channel 4 compare value */
		
		// /* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
		// HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
		
		// /* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
		// HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
	// }
// }
	
	
	
	
	
	
	// MOTOR 1
	/* compute the required duty cycle of PWM1 (since this will change) from this voltage */
	// float PWM1_1_duty_cycle = (v/12.0)*10000.0;
	// uint16_t PWM1_2_duty_cycle = 0; 												/* PWM2 is always LOW when running the motor forwards */
	
	/* set the duty cycle of PWM1_1 using the timer (note that (uint16_t) converts the float to uint16_t) */
	// HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);								/* set initial Timer 3, channel 1 compare value */
	// __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (uint16_t)PWM1_1_duty_cycle);
	
	/* set the PWM2 pin to LOW (GPIO_PIN_RESET is LOW) */
	// __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (uint16_t)PWM1_2_duty_cycle);			/* set initial Timer 3, channel 1 compare value */
	
	/* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
	// HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
	
	/* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
	// HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	
	
	// MOTOR 2
	// /* compute the required duty cycle of PWM1 (since this will change) from this voltage */
	// float PWM2_1_duty_cycle = (v1/12.0)/PERIOD;										/* PWM1 changes as an actual PWM signal */
	// uint16_t PWM2_2_duty_cycle = 0; 												/* PWM2 is always LOW when running the motor forwards */
	
	// set for now so that both motors run simultaneously
	// PWM2_1_duty_cycle = PWM1_2_duty_cycle; 	/* motor 2 PWM1 == motor 1 PWM2, THE MOTORS NEED TO RUN IN OPPOSITE DIRECTIONS!!!!! */
	// PWM2_2_duty_cycle = PWM1_1_duty_cycle;  /* motor 2 PWM2 == motor 1 PWM1, SAME DEAL */
	
	/* set the duty cycle of PWM2_1 using the timer (note that (uint16_t) converts the float to uint16_t) */
	// __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_3, (uint16_t)PWM2_1_duty_cycle);		/* set initial Timer 4, channel 3 compare value */
	
	/* set the duty cycle of PWM2_2 using the timer */
	// __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_4, (uint16_t)PWM2_2_duty_cycle);				/* set initial Timer 4, channel 4 compare value */
	
	/* set the ENB pin to LOW (GPIO_PIN_RESET is LOW) */
	// HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
	
	/* set the EN pin to HIGH (GPIO_PIN_SET is HIGH) */
	// HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
	
// }

// function to inject sinusoidal voltage
// void make_sinusoid(float A)
// {
	// /* A is for amplitude */
	
	
	
	
	
	
	// /* the f is for frequency */
	
	// /* calculate the sin voltage */
	// /* NEED THE TIME INPUT HERE!!!!!!!!!!!!!!! */
	// V = 5*sin((2.0*M_PI*));
	
	// /* put this voltage into  */
	
	// /*  */
	
	
	
// }

































