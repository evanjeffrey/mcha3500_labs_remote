%% run_motor_sim.m used to run the simulation
close all;
clearvars;
clc;



%% define motor objects and parameters
motor = classMotor;
motor.La = 47e-2;
motor.Ra = 0.64;
motor.Km = 1.6;
motor.N = 13;
motor.J = 0.06;
motor.bf = 0.8;
motor.br = 3.4;

%% run simulation
% use ode45
% make use of the state_derivative method - motor.state_derivative(x, Vin, Tl)

% initial conditions
    lambda_0 = 0;
    L_0 = 0;
    theta_0 = 0;
    i_c = [lambda_0; L_0; theta_0];
% inputs
    Vin = @(t) sin(2*t);
    Tl = @(t) 0.003*sin(2*t^2);
% simulation time
    sim_time = [0 10];
% relative tolerance of the solver
    options = odeset('RelTol', 10^(-6));
% use an ode wrapper
    sys_wrap = @(t, x) [motor.state_derivative(x, Vin(t), Tl(t)); motor.omega(x)];
% use ode45
    [tout, xout] = ode45(sys_wrap, sim_time, i_c, options);
    
lambda = xout(:, 1);
L = xout(:, 2);
theta = xout(:, 3);
    
%% plot the results
figure(1)
subplot(2, 1, 1)
plot(tout, lambda)
legend('\lambda_{a}')
subplot(2, 1, 2)
plot(tout, L)
legend('L')

%% plot output shaft angular velocity and armature current
% convert results from states to w and Ia
% pass this the results state vector (one instance of the state at a time)
for i = 1:length(xout)
    w(i) = motor.omega(xout(i, :));
    Ia(i) = motor.current(xout(i, :));
end
figure(2)
subplot(3, 1, 1)
plot(tout, Ia)
legend('I_{a}')
subplot(3, 1, 2)
plot(tout, w)
legend('w')
subplot(3, 1, 3)
plot(tout, theta)
legend('theta')



