%% start fresh
close all;
clearvars;
clc;

% pendulum_sys_id

%% data
% load the data
load('pendData_1')

% unpack data
time = potData.time;
voltage = potData.voltage;

% compute angle in radians
theta = (voltage - 2.125)*(240/3.3)*(pi/180);

% plot data to see
figure(1)
plot(time, theta, 'r+')
xlabel('Time')
ylabel('{\theta} [rads]')
grid on
grid minor

% define initial guess at parameters
% [l; b_theta; theta0; dtheta0]
params_0 = [1; 1; -0.25; 0];

% pack up measured parameters
y_measured = theta;

% run simulation with initial conditions
y = pendSim(time, params_0);

% define cost to be a function of initial conditions and parameters
J_cost = @(params) cost(y_measured, pendSim(time, params));

% run optimisation for system identification
params_opt = fminsearch(J_cost, params_0, optimset('Display', 'iter'));

% run simulation with the optimal parameters
y_simmed = pendSim(time, params_opt);

% plot the results
figure(1)
hold on
plot(time, y_simmed, 'k', 'LineWidth', 2)
legend('Measured', 'optimal sim')

% display the found optimal parameters
% disp("")

%% create a cost function to determine the closeness of the measured and simulated data
function J_cost = cost(y_measured, y_simulated)
    J_cost = norm(y_measured - y_simulated);
end

%% function to determine simulated angles from sample times and system parameters
function y = pendSim(sim_time, params)
    % unpack parameters
    l = params(1);
    b_theta = params(2);
    theta0 = params(3);     % initial condition
    dtheta0 = params(4);    % initial condition
    
    % define known parameters
    mp = 0.035;             % mass of the pendulum [kg]
    g = 9.81;               % acceleration due to gravity
    
    % define J
    J = mp*(l^2);
    
    % define state space equations
    ddtheta = @(x) -(1/J)*(mp*g*l*sin(x(2)) + b_theta*x(1));
    dtheta = @(x) x(1);
    
    % pack state derivative
    dx = @(t, x) [ddtheta(x); dtheta(x)];
    
    % define initial conditions
    i_c = [dtheta0; theta0];
    
    % run simulation
    [res.t, res.x] = ode45(dx, sim_time, i_c, odeset('RelTol', 1e-6));
    
    % unpack resuts vector
    res.theta = res.x(:, 2); % theta is the second state
    
    % pack parameters
    y = res.theta;
end










