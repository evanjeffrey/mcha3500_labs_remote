%% plotting_data_for_report
close all;
clearvars;
clc;

% load data
load("robot_motor2_elec_6V_2Hz_newest");

% unpack data
time = motor2Data.time;
voltage = motor2Data.voltage;
velocity = motor2Data.velocity;
current = motor2Data.current;
N = length(time);

% plot
figure(1)

subplot(3, 1, 1)
plot(time, voltage, '+')
xlabel("Time [s]")
ylabel("Voltage [V]")
grid on
grid minor
grid on

subplot(3, 1, 2)
plot(time, velocity, '+')
xlabel("Time [s]")
ylabel("Velocity [rads/s]")
grid on
grid minor
grid on

subplot(3, 1, 3)
plot(time, current, '+')
xlabel("Time [s]")
ylabel("Current [A]")
grid on
grid minor
grid on

% unpack data
% time = motor1Data.time;
% accAngle = imuData.imuAngle;
% gyroVelocity = imuData.imuVelocity;
% encAngle = imuData.encAngle;
% N = length(time);

% plot
% figure(1)
% 
% subplot(3, 1, 1)
% plot(imuData.time, imuData.imuAngle, '+')
% xlabel("Time [s]")
% ylabel("IMU angle [rads]")
% grid on
% grid minor
% grid on
% 
% subplot(3, 1, 2)
% plot(imuData.time, imuData.imuVelocity, '+')
% xlabel("Time [s]")
% ylabel("IMU velocity [rads/s]")
% grid on
% grid minor
% grid on
% 
% subplot(3, 1, 3)
% plot(imuData.time, imuData.encAngle, '+')
% xlabel("Time [s]")
% ylabel("Encoder angle [rads]")
% grid on
% grid minor
% grid on


