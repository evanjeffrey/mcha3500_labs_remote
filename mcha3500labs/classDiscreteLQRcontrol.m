% a handle class, classDiscreteLQRcontrol

classdef classDiscreteLQRcontrol < handle
   
    properties
    controlMode             % indicator of active control mode
    T                       % sampling period of controller
    A                       % linearised A matrix for controller
    B                       % linearised B matrix for controller
    C_r                     % linearised C matrix for regulation output
    D_r                     % linearised D matrix for regulation output
    A_int                   % A matrix augmented with integrator
    B_int                   % B matrix augmented with integrator
    Q                       % Q matrix for LQR cost
    R                       % R matrix for LQR cost
    N                       % N matrix for LQR cost
    K                       % optimal control gain
    N_x                     % feed-forward gain for states
    N_u                     % feed-forward gain for inputs
    y_ref                   % output regulation reference
    A_z                     % state coefficient for integrator transition dynamics
    B_z                     % input coefficient for integrator transition dynamics
    z                       % integrator state
    end
    
    methods
        
        % function to compute the optimal LQR state-feedback gain
        function [] = controlInit_regulation(obj, A, B, Q, R, N, T)
            % update the values of properties with the input values and
            % compute the discrete time optimal control gain using the
            % provided inputs
            obj.A = A;
            obj.B = B;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            
            % store the control gain in the property K
            obj.K = lqrd(obj.A, obj.B, obj.Q, obj.R, obj.N, obj.T);
            
            % update the propery controlMode with the string 'Regulation'
            obj.controlMode = 'Regulation';     % as in LQRegulation
        end
        
        % function to compute the control action based on the selected
        % control mode
        function u = control(obj, x)
            % compute the control input based on the provided states
            
            % switch between control modes
            switch obj.controlMode
                % regulation only control
                case 'Regulation'
                    u = -obj.K*x;
                    
                % integral action control
                case 'Integrator'
                    % concatinate state vector with integrator state
                    state = [x; obj.z];
                    
                    % compute control signal
                    u = -obj.K*state;
                    
                    % update integrator state for the next iteration
                    obj.z = obj.A_z*state + obj.B_z*u;
                    
                % Reference feed-forward control
                case 'Reference'
                    % compute feed-forward terms u_star and x_star
                    u_star = obj.N_u*obj.y_ref;
                    x_star = obj.N_x*obj.y_ref;
                    
                    % compute control value
                    u = u_star - obj.K*(x - x_star);
                    
                case 'Reference_integrator'
                    % compute feed-forward terms u_star and x_star
                    u_star = obj.N_u*obj.y_ref;
                    x_star = obj.N_x*obj.y_ref;
                    
                    % compute control value
                    u = u_star - obj.K*[(x - x_star); obj.z];
                    
                    % update integrator state for the next iteration
                    % obj.z = obj.z + obj.T*obj.C_r*(x - x_star) + obj.T*obj.D_r*(u - u_star);
                    obj.z = obj.A_z*[(x - x_star); obj.z] + obj.B_z*u;
                    
                % default case if control is un-initialised
                otherwise
                    u = 0;
            end
        end
        
        % disturbance rejection
        function [] = controlInit_integrator(obj, A, B, C_r, D_r, Q, R, N, T)
            % compute the optimal LQR state-feedback gain and integrator
            % dynamics for a specified regulation output
            
            % store properties
            obj.A = A;
            obj.B = B;
            obj.C_r = C_r;
            obj.D_r = D_r;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            
            % construct continuous time augmented A and B matrices with
            % integrator
            obj.A_int = [obj.A, zeros(4, 1); obj.C_r, 0];
            obj.B_int = [obj.B; obj.D_r];
            
            % define the discrete time integrator update matrices using an
            % Euler discretisation, i.e. matrices that satisfy:
            % z_(k+1) = A_z*[x_k - x_star] + B_z*u_k

            obj.A_z = [obj.T*obj.C_r, eye(1)];
            obj.B_z = [obj.T*obj.D_r];
            
            % compute optimal control gain (from the discrete time plant model)
            obj.K = lqrd(obj.A_z, obj.B_z, obj.Q, obj.R, obj.T);
            
            % initialise integrator
            obj.z = 0;
            
            % update indicator
            obj.controlMode = 'Integrator';
        end
        
        function [] = controlInit_reference(obj, A, B, C_r, D_r, Q, R, N, T, y_ref)
            % compute the optimal LQR state-feedback gain ad feed-forward
            % gains for a given reference
            
            % update property values
            obj.A = A;
            obj.B = B;
            obj.C_r = C_r;
            obj.D_r = D_r;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            obj.y_ref = y_ref;
            
            % construct N vector and extract feed-forward gains Nx, Nu
            N_feedforward = [obj.A, obj.B; obj.C_r, obj.D_r]\[zeros(4, 1); eye(1)]; % will be a (5x1)
            obj.N_x = N_feedforward(1:4);   % (take first 4 elements)
            obj.N_u = N_feedforward(5);     % take final element
            
            % compute optimal control gain
            obj.K = lqr(obj.A, obj.B, obj.Q, obj.R);
            
            % update indicator
            obj.controlMode = 'Reference';
        end
        
        function [] = controlInit_reference_integrator(obj, A, B, C_r, D_r, Q, R, N, T, y_ref)
            % compute the optimal LQR state-feedback gain, feed-forward
            % gains and integrator dynamics for a given reference
            
            % update property values
            obj.A = A;
            obj.B = B;
            obj.C_r = C_r;
            obj.D_r = D_r;
            obj.Q = Q;
            obj.R = R;
            obj.N = N;
            obj.T = T;
            obj.y_ref = y_ref;
            
            % construct Nff vector and extract feed-forward gains, N_x and
            % N_u
            N_feedforward = [obj.A, obj.B; obj.C_r, obj.D_r]\[zeros(4, 1); eye(1)]; % will be a (5x5)
            obj.N_x = N_feedforward(1:4);
            obj.N_u = N_feedforward(5);
            
            % construct continuous time augmented A and B matrices with
            % integrator
            obj.A_int = [obj.A, zeros(4, 1); obj.C_r, 0];
            obj.B_int = [obj.B; obj.D_r];
            
            % define the discrete time integrator update matrices using an
            % Euler discretisation, i.e. matrices that satisfy:
            % z_(k+1) = A_z*[x_k - x_star; z_k] + B_z*u_k
            obj.A_z = [obj.T*obj.C_r, eye(1)];
            obj.B_z = [obj.T*obj.D_r];
            
            % compute optimal control gain
            obj.K = lqrd(obj.A_int, obj.B_int, obj.Q, obj.R, obj.T);
            
            % initialise integrator
            obj.z = 0;
            
            % update indicator
            obj.controlMode = 'Reference_integrator';
        end
        
    end
    
end




