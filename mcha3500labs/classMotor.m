% DC motor class
%
% States are assumed to be:
% x = [lambda; L]
% - lambda is armature magnetic flux
% - L is lumped angular momentum
%
% Inputs are:
% Vin - Armature voltage
% Tl - load torque

classdef classMotor
    
    % properties contains all of the parameters for the DC motor class
        properties
            bf          % friction coefficient in forward direction
            br          % friction coefficient in reverse direction
            Ra          % resistance of electrical resistor
            La          % inductance of the electrical inductor
            Km          % motor (gyrator) constant
            N           % gear ratio of the mechanical gearbox
            J           % moment of intertia of the mechanical rotational mass
        end
        % define omega
       
    % methods contains functions related to the DC motor
        methods
            % computes state derivatives given current states and inputs
            % NOTE Tl MUST COME INTO HERE SOMEHOW
            function dx = state_derivative(obj, x, Vin, Tl)
                % unpack states
                lambda = x(1);
                L = x(2);
                
                % expressions for state derivatives
                dlambda = Vin - (obj.Ra/obj.La)*lambda - obj.Km*obj.N*(1/obj.J)*L;
                dL = obj.N*(obj.Km/obj.La)*lambda - obj.friction_torque(x) - Tl;
                
                % pack state derivative
                dx = [dlambda; dL];
            end
            
            % compute the lumped function
            function tau = friction_torque(obj, x)
                % friction torque as a function of angular velocity
                L = x(2);
                omega = (1/obj.J)*L;
                if omega >= 0
                    tau = obj.bf*omega;
                else
                    tau = obj.br*omega;
                end
            end
                
            % compute methods (functions) to compute the armature current
            % and output angular velocity
            
            % compute armature current
            function Ia = current(obj, x)
                lambda = x(1);
                Ia = (1/obj.La)*lambda;
            end
            
            % compute output angular velocity
            function w = omega(obj, x)
                L = x(2);
                w = (1/obj.J)*L;
            end
            
        end
        
end
            
                




