% classBalancingRobotLinear

classdef classBalancingRobotLinear
    
    
    properties
        mc              % mass of the chassis
        Jc              % moment of inertia of the chassis
        mw              % mass of the wheel
        Jw              % moment of inertia of the wheel
        l               % effective length of the chassis
        r               % radius of the wheel
        g               % acceleration due to gravity
        b_phi           % damping coefficient in the phi coordinate
        b_theta         % damping coefficient in the theta coordinate
    end
    
    methods
        
        % note q = [phi; theta] and dq = [dphi; dtheta]
        
        function massMatLin = M(obj)
            % compute and return the linearised mass matrix of the
            % balancing robot system
            M1 = (obj.r^2)*(obj.mw + obj.mc) + obj.Jw;
            M2 = (obj.r)*(obj.r*(obj.mw + obj.mc) + obj.mc*obj.l) + obj.Jw;
            M3 = (obj.r)*(obj.r*(obj.mw + obj.mc) + 2*obj.mc*obj.l) + obj.Jw + obj.Jc + obj.mc*(obj.l^2);
            massMatLin = [M1, M2; M2, M3];
        end
        
        function dampingMatLin = D(obj)
            % compute and return the linearised damping matrix of the
            % balancing robot system
            dampingMatLin = [obj.b_phi, 0; 0, obj.b_theta];
        end
        
        function potGradLin = K(obj)
            % compute and return the matrix containing linearised
            % coefficients of gravitational forces
            potGradLin = [0, 0; 0, -obj.mc*obj.g*obj.l];
        end
        
        function inputMatLin = G(obj)
            % compute and return the inputs mapping matrix of the
            % linearised balancing robot system
            inputMatLin = [1; 0];
        end
        
        % note x = [phi; theta; dphi; dtheta]
        
        function A_lin = A(obj)
            % compute and return the A matrix of the linearised balancing 
            % robot system
            A1 = zeros(2, 2);
            A2 = eye(2);
            A3 = -obj.M()\obj.K();
            A4 = -obj.M()\obj.D();
            A_lin = [A1, A2; A3, A4];
        end
        
        function B_lin = B(obj)
            % compute and return the B matrix of the linearised balncing
            % robot system
            B1 = zeros(2, 1);
            B2 = obj.M()\obj.G();
            B_lin = [B1; B2];
        end
        
        function dx = state_derivative(obj, x, F)
            % compute and return the time derivative of the state vector
            % according to the linearised model
            dx = obj.A()*x + obj.B()*F;
        end
        
        function A_vel_lin = A_vel(obj)
            % compute and return the A matrix of the reduced-order
            % linearised balancing robot system
            A_vel_lin = obj.A();                % extract the matrix from the function
            A_vel_lin = A_vel_lin(2:4, 2:4);    % 'trim' the matrix
        end
        
        function B_vel_lin = B_vel(obj)
            % compute and return the B matrix of the reduced-order
            % linearised balancing robot system
            B_vel_lin = obj.B();                % extract the matrix from the function
            B_vel_lin = B_vel_lin(2:4);         % 'trim' the matrix
        end
        
    end
    
    
    
    
    
    
    
    
    
end
