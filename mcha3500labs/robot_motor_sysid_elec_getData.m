% robot_motor_sysid_elec_getData
close all;
clearvars;
clc;

%% collect data
%% Define STM serial device
% remove any persistant serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end
% Create and connect to device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);
% Reset STM
% txStr = compose("reset");
% fprintf(stm_device, txStr);
% % Catch any print return
% rxStr = fgets(stm_device);

% define amplitude of the sinusoidal voltage
amplitude = 0.04;
% frequency = 2.0;

% run the sinusoidal voltage on the STM
%% for sine torque on motor 1
% command_str = strcat("torque1", " ", num2str(amplitude), " " ,num2str(frequency));

%% for const torque on motor 1
command_str = strcat("torque1", " ", num2str(amplitude));
txStr = compose(command_str);
fprintf(stm_device, txStr);

%% continue

% NOTE: 'compose' appends the correct new-line characters to the string

% get the current values from motor 1
% txStr = compose("logAllmotor2");
% fprintf(stm_device, txStr);

% get the encoder1 values
% txStr = compose("logEncoder1");
% fprintf(stm_device, txStr);

% get the encoder2 values
% txStr = compose("logEncoder2");
% fprintf(stm_device, txStr);

% Define expected number of samples
N = 2000;   % logging for 10 seconds at 200 [Hz]

% Initialise data structure for data
% motor1Data.time = zeros(N,1);
% motor1Data.voltage = zeros(N,1);        % define voltage in advance (hard coded into C at the moment)
% motor1Data.velocity_low = zeros(N,1);
% motor1Data.velocity = zeros(N,1);
% motor1Data.current = zeros(N,1);

motor2Data.time = zeros(N,1);
motor2Data.angle = zeros(N,1);
% motor2Data.velocity_low = zeros(N,1);
motor2Data.velocity = zeros(N,1);
motor2Data.current = zeros(N,1);

% Collect the expected number of samples
for i=1:N
% Read in data from serial connection
rxStr = fgets(stm_device);
% Separate string into parts
separatedString = split(rxStr,',');
% Convert data to numbers and store in data structure
% motor1Data.time(i) = str2double(separatedString{1});
% motor1Data.voltage(i) = 6.0*sin(2.0*2.0*pi*motor1Data.time(i));
% motor1Data.velocity_low(i) = str2double(separatedString{2});
% motor1Data.current(i) = str2double(separatedString{3});
motor2Data.time(i) = str2double(separatedString{1});
% motor2Data.voltage(i) = str2double(separatedString{2});
motor2Data.angle(i) = str2double(separatedString{2});
motor2Data.velocity(i) = str2double(separatedString{3});
motor2Data.current(i) = str2double(separatedString{4});
end

% convert from the low side (the wheels) back to the high side (armature, before the gearbox)
% motor2Data.velocity = 20.4086*(motor2Data.velocity_low);

motor2Data.time = motor2Data.time(5:end);
% motor2Data.voltage = motor2Data.voltage(5:end);
motor2Data.angle = motor2Data.angle(5:end);
motor2Data.velocity = motor2Data.velocity(5:end);
motor2Data.current = motor2Data.current(5:end);

%% Plot results
figure(1)
hold on
% motor 1
subplot(4, 1, 1)        % voltage
plot(motor2Data.time, motor2Data.angle, '+')
title("Motor 1")
xlabel("Time [s]")
ylabel("Angle [rad]")
grid on
grid minor

subplot(4, 1, 2)        % velocity
plot(motor2Data.time, motor2Data.velocity, '+')
xlabel("Time [s]")
ylabel("Velocity [rad/s]")
grid on
grid minor

subplot(4, 1, 3)        % current
plot(motor2Data.time, motor2Data.current, '+')
xlabel("Time [s]")
ylabel("Current [A]")
grid on
grid minor

% also plot calculated torque from current
motor2Data.torque_current = (motor2Data.current).*20.4086.*0.0051;

subplot(4, 1, 4)        % calculated torque
plot(motor2Data.time, motor2Data.torque_current, '+')
xlabel("Time [s]")
ylabel("Recovered torque [Nm]")
grid on
grid minor

% motor 2
% figure(2)
% subplot(3, 1, 1)        % voltage
% plot(motor1Data.time, motor2Data.voltage, '+')
% title("Motor 2")
% xlabel("Time [s]")
% ylabel("Voltage [rad/s]")
% grid on
% grid minor
% 
% subplot(3, 1, 2)        % velocity
% plot(motor1Data.time, motor2Data.velocity, '+')
% xlabel("Time [s]")
% ylabel("Velocity [rad/s]")
% grid on
% grid minor
% 
% subplot(3, 1, 3)        % current
% plot(motor1Data.time, motor2Data.current, '+')
% xlabel("Time [s]")
% ylabel("Current [A]")
% grid on
% grid minor

% subplot(2, 1, 2)
% plot(motor2Data.time, motor2Data.velocity, '+')
% xlabel("Time")
% ylabel("Motor 2 angular velocity")
% grid on
% grid minor

% Teardown
fclose(stm_device);
delete(stm_device);
clear stm_device % Close serial connection and clean up


