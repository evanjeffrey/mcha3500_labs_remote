% robot_motor_sysid_mech_getData
close all;
clearvars;
clc;

%% Define STM serial device
% remove any persistant serial connections
if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end
% Create and connect to device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);

% define amplitude of the sinusoidal voltage
amplitude = 3;
frequency = 1;

% run the sinusoidal voltage on the STM
command_str = strcat("sinVoltage1", " ", num2str(amplitude), " " ,num2str(frequency));
txStr = compose(command_str);
fprintf(stm_device, txStr);

% Define expected number of samples
N = 100;   % logging for 10 seconds at 200 [Hz] 

% pre-allocate data
motor1Data.time = zeros(N,1);
motor1Data.voltage = zeros(N,1);
motor1Data.velocity = zeros(N,1);
motor1Data.current = zeros(N,1);

% Collect the expected number of samples
for i=1:N
    % Read in data from serial connection
    rxStr = fgets(stm_device);
    % Separate string into parts
    separatedString = split(rxStr,',');
%     Convert data to numbers and store in data structure
    motor1Data.time(i) = str2double(separatedString{1});
    motor1Data.voltage(i) =  str2double(separatedString{2});
    motor1Data.velocity(i) = str2double(separatedString{3});
    motor1Data.current(i) = str2double(separatedString{4});
%     motor2Data.time(i) = str2double(separatedString{1});
%     motor2Data.voltage(i) = 6.0*sin(2.0*2.0*pi*motor2Data.time(i));
%     motor2Data.velocity_low(i) = str2double(separatedString{2});
%     motor2Data.current(i) = str2double(separatedString{3});   
end

% Teardown
fclose(stm_device);
delete(stm_device);
clear stm_device % Close serial connection and clean up

%% Plot results
figure(1)
hold on
% motor 1
subplot(3, 1, 1)        % voltage
plot(motor1Data.time, motor1Data.voltage, '+')
title("Motor 1")
xlabel("Time [s]")
ylabel("Voltage [rad/s]")
grid on
grid minor

subplot(3, 1, 2)        % velocity
plot(motor1Data.time, motor1Data.velocity, '+')
xlabel("Time [s]")
ylabel("Velocity [rad/s]")
grid on
grid minor

% subplot(3, 1, 3)        % current
% plot(motor1Data.time, motor1Data.current, '+')
% xlabel("Time [s]")
% ylabel("Current [A]")
% grid on
% grid minor


















