% class classBalancingRobot
% state vector is
% x = [phi; theta; dphi; dtheta]
% x = [q; dq]
% q = [phi; theta]
% dq = [dphi; dtheta]

%% define the class
classdef classBalancingRobot
    %% define the parameters in properties
    properties
        mc              % mass of the chassis
        Jc              % moment of inertia of the chassis
        mw              % combined mass of the wheels
        Jw              % moment of inertia of the wheels
        l               % effective length of the chassis (from pivot to COM)
        r               % radius of each wheel
        g               % acceleration due to gravity
        b_phi           % damping coefficient in phi coordinate
        b_theta         % damping coefficient in theta coordinate
    end
    
    %% create methods (functions) associated with this instance of the class
    methods         
        % calc and return the mass matrix of the balancing robot
        function massMat = M(obj, q, alpha)
            % unpack states
            theta = q(2);
            M1 = (obj.r^2)*(obj.mw + obj.mc) + obj.Jw;
            M2 = obj.r*(obj.r*(obj.mw + obj.mc) + obj.mc*obj.l*cos(alpha + theta)) + obj.Jw;
            M3 = obj.r*(obj.r*(obj.mw + obj.mc) + 2*obj.mc*obj.l*cos(alpha + theta)) + obj.Jw + obj.Jc + obj.mc*(obj.l^2);
            massMat = [M1, M2; M2, M3];
        end
        
        % calc and return the centripetal-Corioilis matrix of the balancing robot
        function corMat = C(obj, q, dq, alpha)
            % unpack states
            theta = q(2);
            dtheta = dq(2);
            corMat = [0, (-dtheta*obj.r*obj.mc*obj.l*sin(alpha + theta)); 0, (-dtheta*obj.r*obj.mc*obj.l*sin(alpha + theta))];
        end
        
        % calc and return the gradient of potential energy wrt to the configuration vector
        function potentialGrad = grav(obj, q, alpha)
            % unpack states
            theta = q(2);
            potentialGrad = [obj.g*obj.r*sin(alpha)*(obj.mw + obj.mc); obj.g*obj.r*sin(alpha)*(obj.mw + obj.mc) - obj.mc*obj.g*obj.l*sin(theta)];
        end
        
        % calc and return the damping matrix of the balacing robot
        function dampingMat = D(obj)
            dampingMat = [obj.b_phi, 0; 0, obj.b_theta];
        end
        
        %% further methods
        % calc and return the kinetic co-energy of the balancing robot
        function kineticCoEng = T(obj, q, dq ,alpha)
            kineticCoEng = (1/2)*dq'*obj.M(q, alpha)*dq;
        end
        
        % calc and return the potential energy of the balancing robot
        function potentialEng = V(obj, q, alpha)
            % unpack states
            phi = q(1);
            theta = q(2);
            potentialEng = obj.g*obj.r*(phi + theta)*sin(alpha)*(obj.mw + obj.mc) + obj.mc*obj.g*obj.l*cos(theta);
        end
        
        % calc and return the time derivative of the state vector
        function dx = state_derivative(obj, x, T, alpha)
            % unpack states
            q = x(1:2);
            dq = x(3:4);
            dv = (obj.M(q, alpha))\([T; 0] - (obj.C(q, dq, alpha) + obj.D())*dq - obj.grav(q, alpha));
            % pack state derivative vector
            dx = [dq; dv];
        end
        
        
    end
    
    
    
    
    
    
    
end









