%% robot_motor_sysid_torque

close all;
clearvars;
clc;

% load data
load('robot_motor2_torqueData_1Hz_newest_2')

% unpack data
time = motorData.time;
current = motorData.current; 
torque = motorData.torque;

% define gear ratio N
N = 20.4086;

%% use sysID to find a suitable number for KI
% define phi matrix
phi = N.*current;                           % just a column vector (N is a constant)

% calculate parameter theta
theta = (phi.' * phi)\(phi.' * torque);     % gives one parameter

% unpack parameters
KI = theta;

% calculate recovered torque
torque_hat = N*KI*current;

%% plot results
figure(1)

subplot(2, 1, 1)
hold on
plot(time, torque, 'b', 'LineWidth', 2)
plot(time, torque_hat, 'k+')
title("1 [Hz]")
xlabel("Time [s]")
ylabel("Torque [Nm]")
grid on
grid minor
legend("measured", "recovered")

%% second dataset validation
% load data
load("robot_motor2_torqueData_2Hz_newest_2");

% unpack data
time_val = motorData.time;
current_val = motorData.current; 
torque_val = motorData.torque;

% calculate recovered torque
torque_hat_val = N*KI*current_val;

subplot(2, 1, 2)
hold on
plot(time_val, torque_val, 'b', 'LineWidth', 2)
plot(time_val, torque_hat_val, 'k+')
title("2Hz validation")
xlabel("Time [s]")
ylabel("Torque [Nm]")
grid on
grid minor
legend("measured", "recovered")























