% robot_chassis_sysid
close all;
clearvars;
clc;

%% define some stuff
% N = 20.4086;
% K_I = 0.0051;
% R_a = 6.337309;
% K_w = 0.305922;

%% load data
load('robot_swing_1Hz')

%% unpack data
time = motor2Data.time;
theta = motor2Data.angle;
dtheta = motor2Data.velocity;
torque = motor2Data.torque_current;

%% correct angle offset
theta = theta - theta(1);

%% chop data
% time = time(400:end);
% theta = theta(400:end);
% dtheta = dtheta(400:end);
% torque = torque(400:end);

%% plot data to see what the go is
figure(1)

subplot(3, 1, 1)
plot(time, theta, '+')
xlabel("Time [s]")
ylabel("\theta [rad]")
grid on
grid minor

subplot(3, 1, 2)
plot(time, dtheta, '+')
% ylim([-1 1])
xlabel("Time [s]")
ylabel("d\theta [rad/s]")
grid on
grid minor

subplot(3, 1, 3)
plot(time, torque, '+')
% ylim([-1 10])
xlabel("Time [s]")
ylabel("Torque [Nm]")
grid on
grid minor

%% perform sysID on the data
% define initial guess at parameters
% [l; b_theta; theta0; dtheta0]

% interpolate the input force
T_in = @(t) interp1(time, torque, t);

params_0 = [0.2; -1.15; 0.01; -0.007];

% pack up measured parameters
y_measured = theta;

% run simulation with initial conditions
y_initial = pendSim(time, params_0, T_in);

% plot with initial parameters
figure(1)
subplot(3, 1, 1)
hold on
plot(time, y_initial, 'r', 'LineWidth', 1)
legend("Measured", "Initial guess")

% define cost to be a function of initial conditions and parameters
J_cost = @(params) cost(y_measured, pendSim(time, params, T_in));

% run optimisation for system identification
params_opt = fminsearch(J_cost, params_0, optimset('Display', 'iter'));

% run simulation with the optimal parameters
y_simmed = pendSim(time, params_opt, T_in);

% plot the results
figure(1)
hold on
subplot(3, 1, 1)
plot(time, y_simmed, 'k', 'LineWidth', 2)
legend('Measured', 'initial guess', 'optimal sim')

% display the found optimal parameters
% disp("")

%% create a cost function to determine the closeness of the measured and simulated data
function J_cost = cost(y_measured, y_simulated)
    J_cost = norm(y_measured - y_simulated);
end

%% function to determine simulated angles from sample times and system parameters
function y = pendSim(sim_time, params, T_in)
    % unpack parameters
    l = params(1);
    b_theta = params(2);
    theta0 = params(3);     % initial condition
    dtheta0 = params(4);    % initial condition
    
    % define known parameters
    mp = 0.7327;             % mass of the chassis [kg]
    g = 9.81;                % acceleration due to gravity
    
    % define J
    J = mp*(l^2);
    
    % define state space equations
    ddtheta = @(T_in, x) (1/J)*(T_in - mp*g*l*sin(x(2)) - b_theta*x(1));
    dtheta = @(x) x(1);
    
    % pack state derivative
    dx = @(t, x) [ddtheta(T_in(t), x); dtheta(x)];
    
    % define initial conditions
    i_c = [dtheta0; theta0];
    
    % run simulation
    [res.t, res.x] = ode45(dx, sim_time, i_c, odeset('RelTol', 1e-6));
    
    % unpack resuts vector
    res.theta = res.x(:, 2); % theta is the second state
    
    % pack parameters
    y = res.theta;
end






